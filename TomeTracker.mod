<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="TomeTracker" version="0.3" date="02/12/2008">
		<Author name="Bloodwalker" email="metagamegeek@gmail.com" />
		<Description text="Tracker Window for your Tome of Knowledge" />
		<Dependencies>
			<Dependency name="EA_ChatWindow"/>
			<Dependency name="EA_TomeOfKnowledge"/>
			<Dependency name="EA_TomeAlertWindow"/>
			<Dependency name="LibSlash" />
			<Dependency name="MapMonster" />
			<Dependency name="LibDateTime-Modified" />
		</Dependencies>
		<Files>
			<File name="Libs/LibStub.lua" />
			<File name="Libs/LibToolkit.lua" />
			<File name="Source/TomeTracker.lua" />
			<!--@alpha@
			<File name="Source/TomeTracker_Debug.lua" />
			@end-alpha@-->
			<!-- Journal Lua Files - Core files first -->
			<File name="Source/TomeTracker_Journal.lua" />
			<File name="Source/TomeTracker_Data.lua" />
			<File name="Source/TomeTracker_Saga.lua" />
			<File name="Source/TomeTracker_Spoilers.lua" />
			<File name="Source/TomeTracker_Unlocks.lua" />
			<File name="Source/TomeTracker_Settings.lua" />
			<File name="Source/TomeTracker_Help.lua" />
			<!-- Journal Window XML Files - Main Window last always-->
			<File name="Source/TomeTracker_Templates.xml" />
			<File name="Source/TomeTracker_Saga.xml" />
			<File name="Source/TomeTracker_Spoilers.xml" />
			<File name="Source/TomeTracker_Unlocks.xml" />
			<File name="Source/TomeTracker_Settings.xml" />
			<File name="Source/TomeTracker_Help.xml" />
			<File name="Source/TomeTracker_Journal.xml" />
			<!-- Tooltip Window files - independant but used by Journal -->
			<File name="Source/TomeTracker_Tooltips.xml" />
			<File name="Source/TomeTracker_Tooltips.lua" />
			<!-- MapMonster handlers and functions -->
			<File name="Source/TomeTracker_Pins.lua" />
		</Files>
		<OnInitialize>
			<!--@alpha@
			<CallFunction name="TomeTracker_Debug.Initialize" />
			@end-alpha@-->
			<CallFunction name="TomeTracker.Initialize" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="TomeTracker_Tooltip_Update" />
		</OnUpdate>
		<OnShutdown>
			<CallFunction name="TomeTracker.OnShutdown" />
		</OnShutdown>
		<SavedVariables>
			<SavedVariable name="TomeTracker_Data" />
			<SavedVariable name="TomeTracker_OrphanedData" />
			<SavedVariable name="TomeTracker.SavedVariables" />
		</SavedVariables>
	</UiMod>
</ModuleFile>
