<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="LibMapPins" version="0.2" date="23/11/2008" autoenabled="false">
		<Author name="Bloodscar" email="" />
		<Description text="Deprecated - Replaced by MapMonster" />

		<Files>
			<File name="Source/LibMapPins_Deprecated.lua" />
		</Files>

		<SavedVariables>
			<SavedVariable name="LibMapPins.PinTypes" />
			<SavedVariable name="LibMapPins.PinSubTypes" />
			<SavedVariable name="LibMapPins.PinStorage" />
			<SavedVariable name="LibMapPins.Settings" />
			<SavedVariable name="LibMapPins_CalibratedOffset" />
			<SavedVariable name="LibMapPins_CalibratedMapSize" />
		</SavedVariables>
	</UiMod>
</ModuleFile>