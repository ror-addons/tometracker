--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Pins.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------

if not TomeTracker or not TomeTracker.Journal then
	if TomeTracker then
		TomeTracker.error("MapMonster Pins Module not loaded")
	end
	return -- the Tometracker Journal messed up somewhere stop loading
end

local LibDateTime = LibStub("LibDateTime-Modified-0.4")
local LibToolkit = LibStub("LibToolkit-0.1")

TomeTracker.Pins = {}

--[===[@alpha@
TomeTracker.Pins.REV = 1
--@end-alpha@]===]
--@non-alpha@
TomeTracker.Pins.REV = 66
--@end-non-alpha@

TomeTracker.Pins.CHARACTER_STATIC   = "TomeTrackerStaticCharacter"
TomeTracker.Pins.CHARACTER_VARIABLE = "TomeTrackerVariableCharacter"
TomeTracker.Pins.STATIC             = "TomeTrackerStatic"
TomeTracker.Pins.VARIABLE           = "TomeTrackerVariable"

TomeTracker.Pins.SECTION_RANKUP = 0


--------------------------------------------------------------------------------
--#
--#			Local Functions
--#
--------------------------------------------------------------------------------

-- local cached data - Unused for the moment
local OtherRealmCache = {}
local BothRealmCache = {}
local rebuildPinCache = true
local lastRealm = -1

--------------------------------------------------------------------------------
--#
--#			Local PinType and SubType Definitions for MapMonster
--#
--------------------------------------------------------------------------------
local PinTypeOptions = {
						label = L"Tome Tracker",
						radius = 4000, -- distance from current pin to be eligible to merge
						isLocked = false, -- default lock status for new pins of this type
						mapIcon = { texture = "EA_HUD_01", -- default icon for map
									scale = 0.65,
									slice = "RvR-Flag", },
						mouseClickCallback = "TomeTracker.Pins.OnMouseClickCallback", -- call this function on left click of map pin
						deleteCallback = "TomeTracker.Pins.OnDeletePinCallback", -- call this function when one of our pins is deleted
						editCallback = "TomeTracker.Pins.OnEditPinCallback", -- call this function when one of our pins is deleted
						mergeHandler = "TomeTracker.Pins.MergeHandler"
						}

local order = tostring(GameData.Realm.ORDER)
local destruction = tostring(GameData.Realm.DESTRUCTION)
local both = tostring(GameData.Realm.NONE)

local PinSubTypes = {}
-- Order SubTypes
PinSubTypes[tostring(TomeTracker.Pins.SECTION_RANKUP) .. order]          = { label = L"Order - Rank Up", }
PinSubTypes[tostring(GameData.Tome.SECTION_BESTIARY) .. order]           = { label = L"Order - Bestiary Unlock",    mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_BESTIARY, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_NOTEWORTHY_PERSONS) .. order] = { label = L"Order - Noteworthy Persons", mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_NOTEWORTHY_PERSONS, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_HISTORY_AND_LORE) .. order]   = { label = L"Order - History & Lore",     mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_HISTORY_AND_LORE, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_OLD_WORLD_ARMORY) .. order]   = { label = L"Order - Armory Unlock",      mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_OLD_WORLD_ARMORY, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_WAR_JOURNAL) .. order]        = { label = L"Order - War Journal",        mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_WAR_JOURNAL, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_PLAYER_TITLES) .. order]      = { label = L"Order - Title Unlock",       mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_PLAYER_TITLES, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_TACTICS) .. order]            = { label = L"Order - Tactic Unlock",      mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_TACTICS, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_ACHIEVEMENTS) .. order]       = { label = L"Order - Achievement",        mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_ACHIEVEMENTS, false), }, }
-- Destruction SubTypes
PinSubTypes[tostring(TomeTracker.Pins.SECTION_RANKUP) .. destruction]          = { label = L"Destruction - Rank Up", }
PinSubTypes[tostring(GameData.Tome.SECTION_BESTIARY) .. destruction]           = { label = L"Destruction - Bestiary Unlock",    mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_BESTIARY, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_NOTEWORTHY_PERSONS) .. destruction] = { label = L"Destruction - Noteworthy Persons", mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_NOTEWORTHY_PERSONS, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_HISTORY_AND_LORE) .. destruction]   = { label = L"Destruction - History & Lore",     mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_HISTORY_AND_LORE, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_OLD_WORLD_ARMORY) .. destruction]   = { label = L"Destruction - Armory Unlock",      mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_OLD_WORLD_ARMORY, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_WAR_JOURNAL) .. destruction]        = { label = L"Destruction - War Journal",        mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_WAR_JOURNAL, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_PLAYER_TITLES) .. destruction]      = { label = L"Destruction - Title Unlock",       mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_PLAYER_TITLES, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_TACTICS) .. destruction]            = { label = L"Destruction - Tactic Unlock",      mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_TACTICS, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_ACHIEVEMENTS) .. destruction]       = { label = L"Destruction - Achievement",        mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_ACHIEVEMENTS, false), }, }
-- Both SubTypes
--[[ Forget the shared entire for now
PinSubTypes[tostring(TomeTracker.Pins.SECTION_RANKUP) .. both]          = { label = L"Both Realms - Rank Up", }
PinSubTypes[tostring(GameData.Tome.SECTION_BESTIARY) .. both]           = { label = L"Both Realms - Bestiary Unlock",    mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_BESTIARY, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_NOTEWORTHY_PERSONS) .. both] = { label = L"Both Realms - Noteworthy Persons", mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_NOTEWORTHY_PERSONS, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_HISTORY_AND_LORE) .. both]   = { label = L"Both Realms - History & Lore",     mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_HISTORY_AND_LORE, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_OLD_WORLD_ARMORY) .. both]   = { label = L"Both Realms - Armory Unlock",      mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_OLD_WORLD_ARMORY, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_WAR_JOURNAL) .. both]        = { label = L"Both Realms - War Journal",        mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_WAR_JOURNAL, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_PLAYER_TITLES) .. both]      = { label = L"Both Realms - Title Unlock",       mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_PLAYER_TITLES, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_TACTICS) .. both]            = { label = L"Both Realms - Tactic Unlock",      mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_TACTICS, false), }, }
PinSubTypes[tostring(GameData.Tome.SECTION_ACHIEVEMENTS) .. both]       = { label = L"Both Realms - Achievement",        mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_ACHIEVEMENTS, false), }, }
--]]

local characterSubTypes = {}
characterSubTypes[tostring(TomeTracker.Pins.SECTION_RANKUP)]          = { label = L"Rank Up", }
characterSubTypes[tostring(GameData.Tome.SECTION_BESTIARY)]           = { label = L"Bestiary Unlock",    mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_BESTIARY, false), }, }
characterSubTypes[tostring(GameData.Tome.SECTION_NOTEWORTHY_PERSONS)] = { label = L"Noteworthy Persons", mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_NOTEWORTHY_PERSONS, false), }, }
characterSubTypes[tostring(GameData.Tome.SECTION_HISTORY_AND_LORE)]   = { label = L"History & Lore",     mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_HISTORY_AND_LORE, false), }, }
characterSubTypes[tostring(GameData.Tome.SECTION_OLD_WORLD_ARMORY)]   = { label = L"Armory Unlock",      mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_OLD_WORLD_ARMORY, false), }, }
characterSubTypes[tostring(GameData.Tome.SECTION_WAR_JOURNAL)]        = { label = L"War Journal",        mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_WAR_JOURNAL, false), }, }
characterSubTypes[tostring(GameData.Tome.SECTION_PLAYER_TITLES)]      = { label = L"Title Unlock",       mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_PLAYER_TITLES, false), }, }
characterSubTypes[tostring(GameData.Tome.SECTION_TACTICS)]            = { label = L"Tactic Unlock",      mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_TACTICS, false), }, }
characterSubTypes[tostring(GameData.Tome.SECTION_ACHIEVEMENTS)]       = { label = L"Achievement",        mapIcon = { texture = "EA_Tome", scale = 1.08, slice = DataUtils.GetTomeSectionIcon(GameData.Tome.SECTION_ACHIEVEMENTS, false), }, }

--[[ Useless for now
local function lookForOtherPins(pinLabel, pinPos, pinRealm)

	if rebuildPinCache or pinRealm ~= lastRealm then
		local pinList = {}
		pinList[TomeTracker.Pins.STATIC] = MapMonsterAPI.GetPinsByType(TomeTracker.Pins.STATIC)
		pinList[TomeTracker.Pins.VARIABLE] = MapMonsterAPI.GetPinsByType(TomeTracker.Pins.VARIABLE)
		
		OtherRealmCache = {}
		BothRealmCache = {}
		
		local otherRealm
		local bothRealm = tostring(GameData.Realm.NONE)
		if pinRealm == GameData.Realm.DESTRUCTION then
			otherRealm = tostring(GameData.Realm.ORDER)
		else
			otherRealm = tostring(GameData.Realm.DESTRUCTION)
		end

		-- build realm caches
		for pinType, pinTable in pairs(pinList) do
			for _, pinData in pairs(pinTable) do
				-- if its from a different realm
				if string.match(pinData.subType, tostring(otherRealm) .. "$") then
					OtherRealmCache[pinData.id] = pinData
				elseif string.match(pinData.subType, tostring(bothRealm) .. "$") then
					BothRealmCache[pinData.id] = pinData
				end
			end
		end
		rebuildPinCache = false
		lastRealm = pinRealm
	end

	-- search the cache for similar pin in other realm
	for pinId, pinData in pairs(OtherRealmCache) do
		-- matching labels
		if pinData.label == pinLabel then
			local mergeRadius = MapMonsterAPI.GetPinTypeOption(pinData.pinType, pinData.subType, "radius")
			local distance = MapMonsterAPI.GetDistance( pinData, pinPos )
			if ( distance <= mergeRadius ) then
				-- label and distance good for match, found a shared realm unlock
				return pinData.Data.realm
			end
		end
	end

	-- search the cache for similar pin in shared realm unlocks
	for pinId, pinData in pairs(BothRealmCache) do
		-- matching labels
		if pinData.label == pinLabel then
			local mergeRadius = MapMonsterAPI.GetPinTypeOption(pinData.pinType, pinData.subType, "radius")
			local distance = MapMonsterAPI.GetDistance( pinData, pinPos )
			if ( distance <= mergeRadius ) then
				-- label and distance good for match, found a shared realm unlock
				return pinData.Data.realm
			end
		end
	end

	-- made it the whole way through we didnt find anything
	return false
end
--]]

local function MigratePinsToMapMonster()

	--[===[@alpha@
	d("*** Migration to MapMonster ***")
	--@end-alpha@]===]

	-- process Tome Unlocks
	for unlockId, unlockData in pairs(TomeTracker_Data.TomeUnlocks) do
		-- drop all positions from tome unlocks and add container for mapmonster pins
		TomeTracker_Data.TomeUnlocks[unlockId].position = nil
		TomeTracker_Data.TomeUnlocks[unlockId].MapMonsterPins = {}
		-- fix labels
		local newLabel = wstring.gsub(unlockData.label, L"%^n,in", L"")
		TomeTracker_Data.TomeUnlocks[unlockId].label = LibToolkit.CleanWString(newLabel)
	end

	-- Convert player map pins to MapMonster
	for server, characters in pairs(TomeTracker_Data.PlayerData) do
		local subTypeSuffix = tostring(TomeTracker_Data.PlayerData[server].Realm)
		for character, characterData in pairs(characters) do
			if character ~= "Realm" then
				-- Convert pins from the epic
				for unlockId, unlockData in pairs(characterData.Epic) do
					-- watch out for broken tome unlocks
					if TomeTracker_Data.TomeUnlocks[unlockId] then
						local pinType = TomeTracker.Pins.VARIABLE
						local subType = TomeTracker_Data.TomeUnlocks[unlockId].section .. subTypeSuffix
						local label
						if unlockId == 24001 then
							label = wstring.gsub(unlockData.label, L"%^%a,%a%a", L"")
							characterData.Epic[unlockId].label = label
						else
							label = TomeTracker_Data.TomeUnlocks[unlockId].label
						end
						local unlockPos
						local note
						local addonData = { unlockId = unlockData.id, linkId = TomeTracker_Data.TomeUnlocks[unlockId].linkId, realm = TomeTracker_Data.PlayerData[server].Realm }
						local splitLoc = WStringSplit(unlockData.locString, L" - ")
						local splitPos = {}
						-- if we have a locString take it apart
						--d(splitLoc)
						if splitLoc[2] ~= L"Unknown" then
							-- disect locString
							for i = 1, 300 do
								if splitLoc[1] == LibToolkit.GetZoneName(i) then
									splitPos.zoneId = i
									break
								end
							end
							if splitPos.zoneId then
								splitPos.zoneX, splitPos.zoneY = string.match(WStringToString(splitLoc[2]), "(%d+.?%d?)K, (%d+.?%d?)K")
							--d(splitPos)
								splitPos.zoneX = LibToolkit.roundNum(tonumber(splitPos.zoneX) * 1000)
								splitPos.zoneY = LibToolkit.roundNum(tonumber(splitPos.zoneY) * 1000)
							end
						end
						-- fix the locString to something perty
						TomeTracker_Data.PlayerData[server][character].Epic[unlockId].locString = LibToolkit.CleanWString(splitLoc[1]) .. L" - " .. splitLoc[2]
						-- process the position data
						if unlockData.isShared then
							pinType = TomeTracker.Pins.STATIC
							note = "Created by Tome Tracker"
							if splitLoc[2] == L"Unknown" then
								-- share position unlock but the locString has no data
							else
								-- make a new pin from locString pos
								-- forget the shared positions since they could be way off from merging
								unlockPos = MapMonsterAPI.ZoneToWorld(splitPos)
							end
						elseif unlockData.position and 
							unlockData.position.worldX > 0 and 
							unlockData.position.worldY > 0 
						then -- not shared and good pos
							note = "Unlocked by " .. character .. " of " .. server
							-- make sure use rounded numbers
							for k, pos in pairs(unlockData.position) do
								unlockData.position[k] = LibToolkit.roundNum(pos)
							end
							-- get fresh zone pos data
							unlockPos = MapMonsterAPI.WorldToZone(unlockData.position)
						else -- not shared and bad pos
							-- stays as unknown we have nothing to work with
						end
						-- drop any position data we have
						TomeTracker_Data.PlayerData[server][character].Epic[unlockId].position = nil
						-- did we manage to get a position for a new pin make it
						if unlockPos then
						
--[[ Forget the shared pins for now it gets tricky
just to save a few entries in most cases
							local foundOther = lookForOtherPins(label, unlockPos, addonData.realm)
							if foundOther then
								subType = TomeTracker_Data.TomeUnlocks[unlockId].section .. tostring(foundOther)
								addonData.realm = 0
							end
--]]
							local pinId = MapMonsterAPI.CreatePin(pinType, subType, label, unlockPos, note, addonData, true, unlockData.datestamp)
							if pinId then
								TomeTracker_Data.PlayerData[server][character].Epic[unlockId].mapMonsterPin = pinId
								-- add pinId for all pins that belong to this unlock
								TomeTracker_Data.TomeUnlocks[unlockId].MapMonsterPins[pinId] = true
							else
								d("Bad Pin Info for: " .. character .. " => " .. unlockId)
								d(MapMonsterAPI.GetError())
							end
						end
					else
						-- found a broken unlock get rid of it
						TomeTracker_Data.PlayerData[server][character].Epic[unlockId] = nil
					end
				end
				-- change the data version to 2 to indicate using MapMonster pins now
				TomeTracker_Data.PlayerData[server][character].DATA_VERSION = 2
			end
		end
	end
	
	-- trash the migration flag
	TomeTracker_Data.migrateToMapMonster = nil
	
end

local function ChangeCharacterPins()

	-- first migration isnt done yet!
	if TomeTracker_Data.migrateToMapMonster then
		return
	end
	--[===[@alpha@
	d("Change character pin types")
	--@end-alpha@]===]
	-- set current character static pins to realm static
	local currentPins = MapMonsterAPI.GetPinsByType(TomeTracker.Pins.CHARACTER_STATIC)
	if currentPins then
		local subTypeSuffix = tostring(TomeTracker_Data.Options.lastRealm)
		for k, pinData in pairs(currentPins) do
			local newProperties = { pinType = TomeTracker.Pins.STATIC, subType = pinData.subType .. subTypeSuffix, }
			MapMonsterAPI.SetPinProperty(pinData.id, newProperties, nil, false)
		end
	end
	
	-- set current character variable pins to realm variable
	local currentPins = MapMonsterAPI.GetPinsByType(TomeTracker.Pins.CHARACTER_VARIABLE)
	if currentPins then
		local subTypeSuffix = tostring(TomeTracker_Data.Options.lastRealm)
		for k, pinData in pairs(currentPins) do
			local newProperties = { pinType = TomeTracker.Pins.VARIABLE, subType = pinData.subType .. subTypeSuffix, }
			MapMonsterAPI.SetPinProperty(pinData.id, newProperties, nil, false)
		end
	end

	local subTypeSuffix = tostring(GameData.Player.realm)
	local subTypeSearch = {}
	subTypeSearch["0" .. subTypeSuffix]  = "0"
	subTypeSearch["1" .. subTypeSuffix]  = "1"
	subTypeSearch["2" .. subTypeSuffix]  = "2"
	subTypeSearch["3" .. subTypeSuffix]  = "3"
	subTypeSearch["4" .. subTypeSuffix]  = "4"
	subTypeSearch["20" .. subTypeSuffix] = "20"
	subTypeSearch["25" .. subTypeSuffix] = "25"
	subTypeSearch["26" .. subTypeSuffix] = "26"
	subTypeSearch["30" .. subTypeSuffix] = "30"
	-- search the saga list for all pins to set as new character pins
	for k, unlockId in pairs(TomeTracker.Player.Chronology) do
		if TomeTracker.Player.Epic[unlockId] and 
		   TomeTracker.Player.Epic[unlockId].mapMonsterPin and
		   type(TomeTracker.Player.Epic[unlockId].mapMonsterPin) == "number"
		then
			local newProperties = {}
			if TomeTracker.Player.Epic[unlockId].isShared then
				newProperties.pinType = TomeTracker.Pins.CHARACTER_STATIC
			else
				newProperties.pinType = TomeTracker.Pins.CHARACTER_VARIABLE
			end
			newProperties.subType = tostring(TomeTracker_Data.TomeUnlocks[unlockId].section)
			MapMonsterAPI.SetPinProperty(TomeTracker.Player.Epic[unlockId].mapMonsterPin, newProperties, nil, false)
		end		
		
	end
		
end

--------------------------------------------------------------------------------
--#
--#			Global Functions and Event Handlers
--#
--------------------------------------------------------------------------------

function TomeTracker.Pins.Initialize()

	d("Initializing Map Pins Module rev " .. TomeTracker.Pins.REV)

	-- Create the player static location pin types
	PinTypeOptions.label = L"Tome Tracker - Character Static"
	MapMonsterAPI.CreatePinType(TomeTracker.Pins.CHARACTER_STATIC, PinTypeOptions, false)
	for subType, subTypeOptions in pairs (characterSubTypes) do
		MapMonsterAPI.CreatePinSubType(TomeTracker.Pins.CHARACTER_STATIC, subType, subTypeOptions)
	end
	MapMonsterAPI.SetPinTypeOption("defaultSubType", tostring(TomeTracker.Pins.SECTION_RANKUP), TomeTracker.Pins.CHARACTER_STATIC)

	-- Create the player variable location pin types
	PinTypeOptions.label = L"Tome Tracker - Character Variable"
	MapMonsterAPI.CreatePinType(TomeTracker.Pins.CHARACTER_VARIABLE, PinTypeOptions, false)
	for subType, subTypeOptions in pairs (characterSubTypes) do
		MapMonsterAPI.CreatePinSubType(TomeTracker.Pins.CHARACTER_VARIABLE, subType, subTypeOptions)
	end
	MapMonsterAPI.SetPinTypeOption("defaultSubType", tostring(TomeTracker.Pins.SECTION_RANKUP), TomeTracker.Pins.CHARACTER_VARIABLE)

	-- Create the realm static location pin types
	PinTypeOptions.label = L"Tome Tracker - Static Unlocks"
	MapMonsterAPI.CreatePinType(TomeTracker.Pins.STATIC, PinTypeOptions, false)
	for subType, subTypeOptions in pairs (PinSubTypes) do
		MapMonsterAPI.CreatePinSubType(TomeTracker.Pins.STATIC, subType, subTypeOptions)
	end
	MapMonsterAPI.SetPinTypeOption("defaultSubType", tostring(TomeTracker.Pins.SECTION_RANKUP), TomeTracker.Pins.STATIC)

	-- Create the realm variable location pin types
	PinTypeOptions.label = L"Tome Tracker - Variable Unlocks"
	MapMonsterAPI.CreatePinType(TomeTracker.Pins.VARIABLE, PinTypeOptions, false)
	for subType, subTypeOptions in pairs (PinSubTypes) do
		MapMonsterAPI.CreatePinSubType(TomeTracker.Pins.VARIABLE, subType, subTypeOptions)
	end
	MapMonsterAPI.SetPinTypeOption("defaultSubType", tostring(TomeTracker.Pins.SECTION_RANKUP), TomeTracker.Pins.VARIABLE)

	-- Register to migrate data on load end if necessary
	if TomeTracker_Data.migrateToMapMonster then
		RegisterEventHandler(SystemData.Events.LOADING_END, "TomeTracker.Pins.OnLoadingEndMigration")
	end
	
	-- Change character pins if weve logged in with a different character since last time
	if TomeTracker_Data.Options.lastCharacter == "" then
		-- dont bother we didnt have a last character
	elseif 
	   TomeTracker_Data.Options.lastCharacter ~= LibToolkit.Clean.PlayerNameW or
	   TomeTracker_Data.Options.lastServer ~= LibToolkit.Clean.ServerNameW or
	   TomeTracker_Data.Options.lastRealm ~= LibToolkit.Realm
	then
		ChangeCharacterPins()
	end
	
	TomeTracker_Data.Options.lastCharacter = LibToolkit.Clean.PlayerNameW
	TomeTracker_Data.Options.lastServer = LibToolkit.Clean.ServerNameW
	TomeTracker_Data.Options.lastRealm = LibToolkit.Realm

end

function TomeTracker.Pins.OnLoadingEndMigration()
	--[===[@alpha@
	d("***On loading migration***")
	--@end-alpha@]===]
	if TomeTracker_Data.migrateToMapMonster then
		MigratePinsToMapMonster()
		ChangeCharacterPins()
	end
end

function TomeTracker.Pins.CreateMapMonsterPin(alertId, position)

	local cleanPos = MapMonsterAPI.ValidateZonePos(position)
	if not cleanPos then
		table.insert(TomeTracker.Saga.needPosUpdate, alertId)
		return
	end
	
	local realm = tostring(GameData.Player.realm)
	local pinType
	local charPinType
	local note
	if TomeTracker.TomeUnlocks[alertId].isShared then
		pinType = TomeTracker.Pins.STATIC
		note = L"Created by Tome Tracker"
		charPinType = TomeTracker.Pins.CHARACTER_STATIC
	else
		pinType = TomeTracker.Pins.VARIABLE
		note = L"Unlocked by " .. LibToolkit.Clean.PlayerNameW .. L" of " .. LibToolkit.Clean.ServerNameW
		charPinType = TomeTracker.Pins.CHARACTER_VARIABLE
	end
	local subType = tostring(TomeTracker.TomeUnlocks[alertId].section) .. realm
	local charSubType = tostring(TomeTracker.TomeUnlocks[alertId].section)
	local label = TomeTracker.TomeUnlocks[alertId].label
	if alertId == 24001 then
		label = TomeTracker.Player.Epic[alertId].label
	end
	local addonData = { linkId = TomeTracker.TomeUnlocks[alertId].linkId, 
						unlockId = alertId,
						realm = GameData.Player.realm, }

	local mapPinId = MapMonsterAPI.CreatePin(pinType, subType, label, cleanPos, note, addonData, true)

	-- Update player epic entry
	TomeTracker.Player.Epic[alertId].mapMonsterPin = mapPinId
	-- Update the tome unlock entry
	TomeTracker.TomeUnlocks[alertId].MapMonsterPins[mapPinId] = true

	-- Now switch it to a character pin
	local newProperties = { pinType = charPinType, subType = charSubType}
	MapMonsterAPI.SetPinProperty(mapPinId, newProperties, nil, false)
	
end

function TomeTracker.Pins.OnMouseClickCallback(pinData)
		
	--[===[@alpha@
	d("MouseClick Callback successfull!")
	--@end-alpha@]===]

	-- Open the Tok to the entry
	local section, entry, subEntry = TomeTracker.Saga.SplitHyperLink( pinData.Data.linkId )
	local entryData = TomeTracker.Saga.FullSaga[pinData.Data.unlockId]
	local unlocked = false

	if entryData then
		unlocked = true
	else
		if section == GameData.Tome.SECTION_BESTIARY then
			if TomeIsBestiarySpeciesUnlocked(entry) then
				unlocked = true
			end
		elseif section == GameData.Tome.SECTION_ACHIEVEMENTS then
			if TomeIsAchievementsEntryUnlocked(entry) then
				unlocked = true
			end
		elseif section == GameData.Tome.SECTION_NOTEWORTHY_PERSONS then
			if TomeIsNoteworthyPersonsEntryUnlocked(entry) then
				unlocked = true
			end
		elseif section == GameData.Tome.SECTION_HISTORY_AND_LORE then
			if TomeIsHistoryAndLoreEntryUnlocked(entry) then
				unlocked = true
			end
		elseif section == GameData.Tome.SECTION_OLD_WORLD_ARMORY then
		elseif section == GameData.Tome.SECTION_WAR_JOURNAL then
			if TomeIsWarJournalEntryUnlocked(entry) then
				unlocked = true
			end
		elseif section == GameData.Tome.SECTION_PLAYER_TITLES then
			if TomeIsPlayerTitleUnlocked(entry) then
				unlocked = true
			end
		elseif section == GameData.Tome.SECTION_TACTICS then
		elseif section == GameData.Tome.SECTION_LIVE_EVENT then
		end
	end

	if unlocked then
		TomeTracker.Saga.OnHyperLinkClicked( pinData.Data.linkId )
	else
		TomeTracker.Saga.OnHyperLinkClicked( L"TOME0:0:0" )
		AlertTextWindow.AddLine(SystemData.AlertText.Types.MOVEMENT_RVR, L"Entry still locked")
		
	end
end

function TomeTracker.Pins.OnDeletePinCallback(pinData)

	--[===[@alpha@
	d("Delete Callback successfull!")
	--@end-alpha@]===]
	--d(pinData)

	-- we have to search the unlocks and delete any references to the deleted pin
	local unlockList = {}
	-- find the unlocks that have this pin
	for unlockId, unlockData in pairs(TomeTracker_Data.TomeUnlocks) do
		if unlockData.MapMonsterPins[pinData.id] then
			-- found one
			-- drop the pin from the unlock record
			unlockData.MapMonsterPins[pinData.id] = nil
			-- add the unlock to list
			table.insert(unlockList, unlockId)
		end
	end
	-- Drop matching pins from player records
	for serverName, playerList in pairs(TomeTracker_Data.PlayerData) do
		for playerName, playerDetails in pairs(playerList) do
			if playerName ~= "Realm" then
				for unlockId, unlockData in pairs(playerDetails.Epic) do
					if unlockData.mapMonsterPin == pinData.id then
						TomeTracker_Data.PlayerData[serverName][playerName].Epic[unlockId].mapMonsterPin = nil
					end
				end
			end
		end
	end
	
	TomeTracker.Saga.UpdateEpicSaga()
	
end

function TomeTracker.Pins.OnEditPinCallback(pinData)

	--[===[@alpha@
	d("Edit Callback successfull!")
	--@end-alpha@]===]
	--d(pinData)

end

function TomeTracker.Pins.MergeHandler(newPinData, existingPinData)

	--[===[@alpha@
	d("Merge Callback successfull!")
	--@end-alpha@]===]
	--d(pinData)
	
	-- always return the new pin data to save as the merged info
	return newPinData
	
end

--------------------------------------------------------------------------------
--#
--#			Recover Map Pins from Tome Tracker Data
--#
--------------------------------------------------------------------------------

function TomeTracker.Pins.Recovery()
	if true then return end
	-- Drop all tome unlock map pins
	for unlockId, unlockData in pairs(TomeTracker_Data.TomeUnlocks) do
		TomeTracker_Data.TomeUnlocks[unlockId].label = LibToolkit.CleanWString(wstring.gsub(TomeTracker_Data.TomeUnlocks[unlockId].label, L"%^%a,in", L""))
		TomeTracker_Data.TomeUnlocks[unlockId].MapMonsterPins = {}
	end

	-- Create map pins from tome unlock locstring
	for server, characters in pairs(TomeTracker_Data.PlayerData) do
		local subTypeSuffix = tostring(TomeTracker_Data.PlayerData[server].Realm)
		for character, characterData in pairs(characters) do
			if character ~= "Realm" then
				-- Convert pins from the epic
				for unlockId, unlockData in pairs(characterData.Epic) do
					-- all the map pins are gone so these are pointless
					TomeTracker_Data.PlayerData[server][character].Epic[unlockId].mapMonsterPin = nil
					-- watch out for broken tome unlocks
					if TomeTracker_Data.TomeUnlocks[unlockId] then
						-- fix for locstring
						TomeTracker_Data.PlayerData[server][character].Epic[unlockId].locString = wstring.gsub(TomeTracker_Data.PlayerData[server][character].Epic[unlockId].locString, L"%^%a,in", L"")
						-- fix for label
						if unlockData.label then
							unlockData.label = LibToolkit.CleanWString(wstring.gsub(unlockData.label, L"%^%a,in", L""))
						end
						local pinType
						local note
						if unlockData.isShared then
							pinType = TomeTracker.Pins.STATIC
							note = "Created by Tome Tracker"
						else
							pinType = TomeTracker.Pins.VARIABLE
							note = "Unlocked by " .. character .. " of " .. server
						end
						local subType = TomeTracker_Data.TomeUnlocks[unlockId].section .. subTypeSuffix
						local label
						if unlockId == 24001 then
							label = unlockData.label
						else
							label = TomeTracker_Data.TomeUnlocks[unlockId].label
						end						
						local addonData = { unlockId = unlockData.id, linkId = TomeTracker_Data.TomeUnlocks[unlockId].linkId, realm = TomeTracker_Data.PlayerData[server].Realm }

						local unlockPos
						local splitLocString = WStringSplit(unlockData.locString, L" - ")
						local splitPos = {}
						-- if we have a locString take it apart
						if splitLocString[2] ~= L"Unknown" then
							-- disect locString
							for i = 1, 300 do
								--local zoneName = wstring.gsub(GetZoneName(i), L"%^%a,in", L"")
								local zoneName = LibToolkit.GetZoneName(i)
								if splitLocString[1] == zoneName then
									splitPos.zoneId = i
									break
								end
							end
							if splitPos.zoneId then
								splitPos.zoneX, splitPos.zoneY = string.match(WStringToString(splitLocString[2]), "(%d+.?%d?)K, (%d+.?%d?)K")
								splitPos.zoneX = LibToolkit.roundNum(tonumber(splitPos.zoneX) * 1000)
								splitPos.zoneY = LibToolkit.roundNum(tonumber(splitPos.zoneY) * 1000)
							else
								d("** Missing zoneId for :")
								d(splitLocString)
							end
							unlockPos = MapMonsterAPI.ZoneToWorld(splitPos)
							-- did we manage to get a position for a new pin make it
							if unlockPos then
								local pinId = MapMonsterAPI.CreatePin(pinType, subType, label, unlockPos, note, addonData, true, unlockData.datestamp)
								if pinId then
									TomeTracker_Data.PlayerData[server][character].Epic[unlockId].mapMonsterPin = pinId
									-- add pinId for all pins that belong to this unlock
									TomeTracker_Data.TomeUnlocks[unlockId].MapMonsterPins[pinId] = true
								else
									d("Bad Pin Info for: " .. character .. " => " .. unlockId)
									d(MapMonsterAPI.GetError())
								end
							else
								d("Bad location info  for: " .. character .. " from " .. server .. " => " .. unlockId)
								d(MapMonsterAPI.GetError())
								d(splitPos)
							end
							
						end
												
					else
						d("**** Broken Unlock!!")
						-- found a broken unlock get rid of it
						TomeTracker_Data.PlayerData[server][character].Epic[unlockId] = nil
					end
				end

			end
		end
	end
	
end

