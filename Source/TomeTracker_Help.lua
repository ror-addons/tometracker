--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Help.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------
if not TomeTracker or not TomeTracker.Journal then
	if TomeTracker then
		TomeTracker.error("Help Module not loaded")
	end
	return -- the Tometracker Journal messed up somewhere stop loading
end

TomeTracker.Help = {}

--[===[@alpha@
TomeTracker.Help.REV = 2
--@end-alpha@]===]
--@non-alpha@
TomeTracker.Help.REV = 66
--@end-non-alpha@


TomeTracker.Help.windowName = "TTHelp"
TomeTracker.Help.VerString = "TomeTracker v" .. TomeTracker.MAJOR_VER .. "." .. TomeTracker.MINOR_VER .. " - by Bloodwalker"

TomeTracker.Help.TextPage = TomeTracker.Help.VerString .. [[


Available slash commands:
  /tometracker - Toggles the TomeTracker Journal Window
  /journal - alias to /tometracker

Simple usefull macros:
  /script TomeTracker.Journal.MapButtonToggle()
  /script TomeTracker.Journal.Toggle()
    *note: Journal window can be closed by hitting ESC

Caveats:
  - Some Saga list entries may report as "Unknown" coordinates if the zone information is incomplete, all entries contain world coordinates and will be updated properly once complete zone information becomes available
  - Cards and bragging rights rewards haven't been tested since they aren't readily available, if you wish to help in testing cards visit the TomeTracker page on curse

For full details and help visit:
  http://war.curse.com/downloads/war-addons/details/tometracker.aspx

Credits and acknowledgements:
- First to all the devs and staff at EA/Mythic for making a great game and whos code I used as a reference to the Warhammer API and UI.
- To many of the coders at curse.com whos code I used as implementation references and inspiration
- Last but not least to the best girl a gaming geek could ask for, who puts up with far too much Warhammer and endless hours of coding and debugging]]


function TomeTracker.Help.Initialize()

		d("Initializing Help Tab rev " .. TomeTracker.Help.REV)

		LabelSetText( TomeTracker.Help.windowName .. "Text", towstring(TomeTracker.Help.TextPage) )
--    WindowSetTintColor( TomeTracker.Help.windowName .. "Background", 128, 20, 20 )
--    WindowSetAlpha( TomeTracker.Help.windowName .. "Background", 1 )

end

--[[

-- All this text can fit in the window as is but not more

TomeTracker.Help.TextPage =
WWWW WWWW WWW WWW WW WWW WWW WWW WWW WWW WW WW
ABCDEFGHIJKLMNOPQRSTUVWXYZ ABC DEF GHI JKL MNO PQR STUVWXYZ A
1234567890 1234567890 1234567890 1234567890 1234567890 12 34 567
B234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 11
111 111 111 111 111 111 111 111 111 111 111 111 111 111 111 111 111 11
C234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
D234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
E234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
F234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
G234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
H234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
J234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
K234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
L234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
M234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
N234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
O234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
P234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
Q234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
R234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
S234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
T234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
U234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
V234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
W234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
X234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
Y234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
Z234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 
A234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 11
B234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 11
C234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 11
D234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 6789 1234 11

--]]