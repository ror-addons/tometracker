--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Journal.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------

if not TomeTracker then
		return -- the Tometracker Core messed up somewhere stop loading
end

-- init Libs
local LibDateTime = LibStub("LibDateTime-Modified-0.4")
local LibToolkit = LibStub("LibToolkit-0.1")

if not LibDateTime then
		if not LibDateTime then
				TomeTracker.error("LibDateTime-Modified NOT loaded")
		end
		TomeTracker.error("Journal Window NOT Loaded")
		return -- Something broke in the Libs we need them for the journal
end

TomeTracker.Journal = {}
TomeTracker.Journal.RegisteredObservers = {}
TomeTracker.Journal.PendingAlerts = {}

TomeTracker.Journal.windowName = "TomeTracker_JournalWindow"

--[===[@alpha@
TomeTracker.Journal.REV = 5
--@end-alpha@]===]
--@non-alpha@
TomeTracker.Journal.REV = 66
--@end-non-alpha@

TomeTracker.Journal.TAB_SAGA = 1
TomeTracker.Journal.TAB_UNLOCKS = 2
TomeTracker.Journal.TAB_SPOILERS = 3
TomeTracker.Journal.TAB_SETTINGS = 4
TomeTracker.Journal.TAB_HELP = 5

TomeTracker.Journal.Tabs = {}
TomeTracker.Journal.Tabs[TomeTracker.Journal.TAB_SAGA]     = { window = "TTSaga",     name = TomeTracker.Journal.windowName .. "TabSaga",     label = L"Saga",     tooltip = L"The Epic Saga of " .. GameData.Player.name .. L" " .. GameData.Player.lastname }
TomeTracker.Journal.Tabs[TomeTracker.Journal.TAB_UNLOCKS]  = { window = "TTUnlocks",  name = TomeTracker.Journal.windowName .. "TabUnlocks",  label = L"Unlocks",  tooltip = L"List of available Tome Unlocks" }
TomeTracker.Journal.Tabs[TomeTracker.Journal.TAB_SPOILERS] = { window = "TTSpoilers", name = TomeTracker.Journal.windowName .. "TabSpoilers", label = L"Spoilers", tooltip = L"Tome Unlock Spoilers\nfrom WHA forum threads" }
TomeTracker.Journal.Tabs[TomeTracker.Journal.TAB_SETTINGS] = { window = "TTSettings", name = TomeTracker.Journal.windowName .. "TabSettings", label = L"Settings", tooltip = L"Tome Tracker Settings UI" }
TomeTracker.Journal.Tabs[TomeTracker.Journal.TAB_HELP]     = { window = "TTHelp",     name = TomeTracker.Journal.windowName .. "TabHelp",     label = L"Help",     tooltip = L"Help File and Instructions" }

function TomeTracker.Journal.Initialize()

	d("Initializing TomeTracker Journal Core rev " .. TomeTracker.Journal.REV)

	-- Make sure we loaded the data file
	if not TomeTracker.Data then
		return
	end

	-- Create journal window
	CreateWindow(TomeTracker.Journal.windowName, false)

	-- Create the overhead map button
	CreateWindow("TomeTracker_JournalMapButton", true)

    -- Register the overhead map button with the Layout Editor
    LayoutEditor.RegisterWindow( "TomeTracker_JournalMapButton",
                                L"TomeTracker Journal MapButton",
                                L"Toggle Button to open Tome Tracker Journal",
                                false, false,
                                true, nil )

	if not TomeTracker.Options.showMapButton then
		WindowSetShowing("TomeTracker_JournalMapButton", false)
	end

	-- Initialize all the tab windows from here
	TomeTracker.Saga.Initialize()
	TomeTracker.Unlocks.Initialize()
	TomeTracker.Spoilers.Initialize()
	TomeTracker.Settings.Initialize()
	TomeTracker.Help.Initialize()
    
    -- Catch the tome alerts as they are fired
    RegisterEventHandler(SystemData.Events.TOME_ALERT_ADDED, "TomeTracker.Journal.OnTomeAlertAdded")
    
    -- Register level and renown change events
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED, "TomeTracker.Journal.OnRenownUpdated")
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RANK_UPDATED, "TomeTracker.Journal.OnCareerRankUpdated")

	-- Register LoadingEnd to process alerts once everything is loaded		
	RegisterEventHandler(SystemData.Events.LOADING_END, "TomeTracker.Journal.OnLoadingEnd")
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "TomeTracker.Journal.OnLoadingEnd")

	-- Set window title
	LabelSetText( TomeTracker.Journal.windowName .. "TitleBarText", L"Tome Tracker Journal" )

	-- Set Tab labels
	for i, tabData in ipairs(TomeTracker.Journal.Tabs) do
		ButtonSetText(tabData.name, tabData.label )
	end

	-- Default to the Help Tab
	local defaultTab = TomeTracker.Options.defaultTab or 5
	TomeTracker.Journal.SelectTab(defaultTab)

	-- Register new slash command
	LibSlash.RegisterSlashCmd("journal", TomeTracker.Journal.SlashHandler)
	LibSlash.RegisterSlashCmd("tometracker", TomeTracker.Journal.SlashHandler)
	
	TomeTracker.print("Journal Window Loaded")		
	
end

--
-- Window Handling Functions
--

function TomeTracker.Journal.Show()
    WindowSetShowing(TomeTracker.Journal.windowName, true)
end

function TomeTracker.Journal.Hide()
    WindowSetShowing(TomeTracker.Journal.windowName, false)
end

function TomeTracker.Journal.Toggle()
    WindowSetShowing(TomeTracker.Journal.windowName, not WindowGetShowing( TomeTracker.Journal.windowName ))
end

function TomeTracker.Journal.MapButtonShow()
	TomeTracker.Options.showMapButton = true
	WindowSetShowing("TomeTracker_JournalMapButton", true)
end

function TomeTracker.Journal.MapButtonHide()
	TomeTracker.Options.showMapButton = false
	WindowSetShowing("TomeTracker_JournalMapButton", false)
end

function TomeTracker.Journal.MapButtonToggle()
	if WindowGetShowing("TomeTracker_JournalMapButton") then
		TomeTracker.Journal.MapButtonHide()
	else
		TomeTracker.Journal.MapButtonShow()
	end
end

function TomeTracker.Journal.OnHidden()
    WindowUtils.OnHidden()
end

function TomeTracker.Journal.OnShown()
    WindowUtils.OnShown(TomeTracker.Journal.Hide, WindowUtils.Cascade.MODE_AUTOMATIC)
end

-- This function hides all the tabbed windows and shows the tabbed window based on the param.
function TomeTracker.Journal.SelectTab(tabNumber)

    for index, TabIndex in ipairs(TomeTracker.Journal.Tabs) do
        if (index == tabNumber) then
            ButtonSetPressedFlag( TabIndex.name, true )
            WindowSetShowing( TabIndex.window, true )
            TomeTracker.Options.defaultTab = tabNumber
        else
            ButtonSetPressedFlag( TabIndex.name, false )
            WindowSetShowing( TabIndex.window, false )
        end
    end
    
end

--
-- Window Event Handlers
--

function TomeTracker.Journal.OnMouseOverTab()
    local windowName	= SystemData.ActiveWindow.name
    local windowIndex	= WindowGetId (windowName)

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1,  TomeTracker.Journal.Tabs[windowIndex].tooltip)
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_HEADING)	
    Tooltips.Finalize()
    
    local anchor = { Point="bottom", RelativeTo=windowName, RelativePoint="top", XOffset=0, YOffset=32 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end


function TomeTracker.Journal.OnLButtonUpTab()
    TomeTracker.Journal.SelectTab(WindowGetId (SystemData.ActiveWindow.name))
end


function TomeTracker.Journal.MapButtonMouseOver()
	-- TODO: quick and dirty not very pretty maybe Ill fix it later

    local windowName	= SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1,  L"Toggle TomeTracker Journal Window")
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

--
-- Game Event Handlers
--

function TomeTracker.Journal.OnCareerRankUpdated()

	--[===[@alpha@
	d("function TomeTracker.Journal.OnCareerRankUpdated()")
	--@end-alpha@]===]
	
	-- shortcut some odd level numbers when loading
	if ( GameData.Player.level < 0 ) then
		return
	end
	
	if (TomeTracker.Player.level == 0) then
		TomeTracker.Player.level = GameData.Player.level
	elseif (TomeTracker.Player.level < GameData.Player.level) then
	
		TomeTracker.Player.level = GameData.Player.level

		-- create new alert because we leveled up!
		local newAlert = {
			-- internal tome alert id are 1 to 12k, I start at 24k
				id = 24000 + TomeTracker.Player.level,
				section = 0,
				entry = 0,
				subEntry = 0,
				name = L"Achieved Level " .. TomeTracker.Player.level,
				desc = L"You've achieved Level " .. TomeTracker.Player.level,			
				rewardId = 0,
				rewardType = 0,
				}

		TomeTracker.Journal.AddTomeAlert( newAlert )

	end

end

function TomeTracker.Journal.OnRenownUpdated()

	--[===[@alpha@
	d("function TomeTracker.Journal.OnRenownUpdated()")
	--@end-alpha@]===]

	-- shortcut some odd renown numbers when loading
	if ( GameData.Player.Renown.curRank < 0 ) then
		return
	end

	if (TomeTracker.Player.renown < GameData.Player.Renown.curRank) then
		TomeTracker.Player.renown = GameData.Player.Renown.curRank

		-- create new alert because we got new renown rank
		local newAlert = {
			-- internal tome alert ids are 1 to 12k, I start at 24k
				id = 24100 + TomeTracker.Player.renown,
				section = 0,
				entry = 0,
				subEntry = 0,
				name = L"Achieved Renown Rank " .. TomeTracker.Player.renown,
				desc = L"You've achieved Renown Rank " .. TomeTracker.Player.renown,
				rewardId = 0,
				rewardType = 0,
				}

		TomeTracker.Journal.AddTomeAlert( newAlert )

	end

end

function TomeTracker.Journal.OnTomeAlertAdded()

	--[===[@alpha@
	d("function TomeTracker.Journal.OnTomeAlertAdded()")
	--@end-alpha@]===]
	local allAlerts = DataUtils.GetTomeAlerts()
	local tomeAlert = LibToolkit.CopyObject(allAlerts[1])

	if (tomeAlert == nil) then
		return -- skip some empty tome alerts during loading
	end

	-- Exclude certain alert types
	if (tomeAlert.section == GameData.Tome.SECTION_ZONE_MAPS or
		tomeAlert.section == GameData.Tome.SECTION_GAME_MANUAL or
		tomeAlert.section == GameData.Tome.SECTION_GAME_FAQ )
	then
		return --  shortcut this function we dont want these
	end

	TomeTracker.Journal.AddTomeAlert( tomeAlert )
	
end

function TomeTracker.Journal.OnLoadingEnd()

	--[===[@alpha@
	d("function TomeTracker.Journal.OnLoadingEnd()")
	--@end-alpha@]===]
	if ( TomeTracker.Journal.PendingAlerts[1] ~= nil ) then
		--[===[@alpha@
		d("Processing PendingAlerts Table")
		--@end-alpha@]===]		
		-- Process pending alerts
		for i, tomeAlert in ipairs(TomeTracker.Journal.PendingAlerts) do
			-- quick fix for renown alert before level set during loading
			if (tomeAlert.level == 0) then
				tomeAlert.level = TomeTracker.Player.level
			end
			TomeTracker.Journal.CallAlertObservers(tomeAlert)
			TomeTracker.Journal.PendingAlerts[i] = nil
		end
	end

end

--
-- Internal Alert functions
--

function TomeTracker.Journal.AddTomeAlert( newAlert )

	--[===[@alpha@
	d("function TomeTracker.Journal.AddTomeAlert(newAlert)")
	--@end-alpha@]===]
	local completeAlert = TomeTracker.Journal.AssembleAlertData( newAlert )

	if (TomeTracker.isWorldLoading) then
		table.insert(TomeTracker.Journal.PendingAlerts, completeAlert)
	else
		TomeTracker.Journal.CallAlertObservers( completeAlert )
	end

end

function TomeTracker.Journal.AssembleAlertData(alertData)

	--[===[@alpha@
	d("function TomeTracker.Journal.AssembleAlertData(alertData)")
	--@end-alpha@]===]
	-- drop unecessary data
	alertData.alertType = nil
	alertData.suppressPopup = nil
	alertData.xp = nil

	-- save player info with the alert
	alertData.level = TomeTracker.Player.level
	alertData.renown = TomeTracker.Player.renown
	alertData.timestamp = LibDateTime.Now()
	
	local playerPos = MapMonsterAPI.GetPlayerPosition()
	if not playerPos then
		playerPos = nil
	end
	alertData.position = playerPos

	alertData.linkId = L"TOME" .. towstring(alertData.section) .. L":" .. towstring(alertData.entry) .. L":" .. towstring(alertData.subEntry)

	-- if its an actual tome section
	if ( alertData.section ~= 0 ) then
		-- Get flag from data table
		alertData.isShared = TomeTracker.Data.Common[alertData.section]
	else
		alertData.isShared = false
	end

	-- if not shared then run another check
	if not alertData.isShared then
		if (alertData.section == GameData.Tome.SECTION_ACHIEVEMENTS) then
			local achievementData = TomeGetAchievementsEntryData(alertData.entry)
			alertData.isShared = TomeTracker.Data.AchievementType[achievementData.typeId]
		elseif (alertData.section == GameData.Tome.SECTION_BESTIARY) then
			-- crude string search for bestiary alerts, just a few arent shared
			local searchString = WStringToString(alertData.name)
			if ( string.match(searchString, "^Encounter") or
				string.match(searchString, "^Kill") or
				string.match(searchString, "^Victim") ) 
			then
				alertData.isShared = false		
			else
				alertData.isShared = true
			end
		end
	end

	return alertData

end

--
--
--
function TomeTracker.Journal.RegisterObserver(observer)

	--[===[@alpha@
	d("function TomeTracker.Journal.RegisterObserver(observer) - Adding Alert Observer")
	--@end-alpha@]===]
	-- Check we that we got a function
	if ( type(observer) == "function" ) then
		--[===[@alpha@
		d("Observer Registered with Journal")
		--@end-alpha@]===]
		table.insert(TomeTracker.Journal.RegisteredObservers, observer)
		return true
	else
		return false
	end
end

--
--
--
function TomeTracker.Journal.RemoveObserver(observer)

	--[===[@alpha@
	d("function TomeTracker.Journal.RemoveObserver(observer) - Removing Alert Observer")
	--@end-alpha@]===]
	-- Check we didnt get an empty string 
	if ( type(observer) == "function" and observer ~= "" ) then
		for i, handler in ipairs(TomeTracker.Journal.RegisteredObservers) do
			if (handler == observer) then
				--[===[@alpha@
				d("Observer Removed from Journal")
				--@end-alpha@]===]
				TomeTracker.Journal.RegisteredObservers[i] = nil
				return true
			end
		end
	end
	
	return false
	
end

function TomeTracker.Journal.CallAlertObservers(tomeAlert)

	--[===[@alpha@
	d("function TomeTracker.Journal.CallAlertObservers(tomeAlert) - Entered observer dispatch")
	--@end-alpha@]===]
	for i, handler in ipairs(TomeTracker.Journal.RegisteredObservers) do
		--[===[@alpha@
		d("Calling Alert Observer:")
		--@end-alpha@]===]
		handler(tomeAlert)
	end
end


function TomeTracker.Journal.SlashHandler(args)
	--[===[@alpha@
	d("slashHandler args=" .. args)
	--@end-alpha@]===]

	local cmd = StringSplit(args)

	if (cmd[1] == "version" or cmd[1] == "ver") then
		TomeTracker.PrintVersion()
	elseif (cmd[1] == "status") then
		TomeTracker.PrintStatus()
	elseif (cmd[1] == "help") then
		TomeTracker.print("TomeTracker Journal Help:")
		TomeTracker.print("  /tometracker [command]")
		TomeTracker.print("    help - Prints this help message to chat window")
		TomeTracker.print("    version - Print the current TomeTracker version to chat window")
		TomeTracker.print("    status - Print current version and any loading errors to chat window")
	else
		-- Defaults to toggling the journal window
		TomeTracker.Journal.Toggle()
	end

end
