--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Saga.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------

if not TomeTracker or not TomeTracker.Journal then
	if TomeTracker then
		TomeTracker.error("Saga Module not loaded")
	end
	return -- the Tometracker Journal messed up somewhere stop loading
end

local LibDateTime = LibStub("LibDateTime-Modified-0.4")
local LibToolkit = LibStub("LibToolkit-0.1")

TomeTracker.Saga = {}

--[===[@alpha@
TomeTracker.Saga.REV = 5
--@end-alpha@]===]
--@non-alpha@
TomeTracker.Saga.REV = 66
--@end-non-alpha@


TomeTracker.Saga.windowName = "TTSaga"
TomeTracker.Saga.havePosError = false
TomeTracker.Saga.isBeta = false

TomeTracker.Saga.FullSaga = {}	
TomeTracker.Saga.storyOrder = {}
TomeTracker.Saga.needPosUpdate = {}
TomeTracker.Saga.needZoneUpdate = {}
TomeTracker.Saga.needRewardUpdate = {}

local firstAddonRun = true

function TomeTracker.Saga.Initialize()

	d("Initializing Saga Tab rev " .. TomeTracker.Saga.REV)	

	-- initial window display
	LabelSetText( TomeTracker.Saga.windowName .. "TitleText", TomeTracker.Journal.Tabs[TomeTracker.Journal.TAB_SAGA].tooltip )
	TomeTracker.Saga.SetListRowTints()		

	TomeTracker.Saga.CheckForFirstRun()

	if firstAddonRun then
		--[===[@alpha@
		d("**** Register Event Handler ENTER_WORLD for TomeTracker.Saga.OnEnterWorld()")
		--@end-alpha@]===]
		RegisterEventHandler(SystemData.Events.ENTER_WORLD, "TomeTracker.Saga.OnEnterWorld")
	end

	RegisterEventHandler(SystemData.Events.LOADING_END, "TomeTracker.Saga.UpdateEpicSaga")
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "TomeTracker.Saga.UpdateEpicSaga")
	RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "TomeTracker.Saga.OnPositionUpdated")

	TomeTracker.Journal.RegisterObserver(TomeTracker.Saga.ProcessAlert)

end

--
-- Event Handlers
--

function TomeTracker.Saga.UpdateEpicSaga()
	
	--[===[@alpha@
	d("function TomeTracker.Saga.UpdateEpicSaga()")
	--@end-alpha@]===]

	-- if we haven't built our full saga list yet do it now
	if not TomeTracker.Saga.FullSaga[24001] then
		TomeTracker.Saga.BuildFullSagaTable()
	end

	-- then we filter unwanted items from the display
	TomeTracker.Saga.FilterEpicSaga()
	-- then we set the list of entries to display
	ListBoxSetDisplayOrder( TomeTracker.Saga.windowName .. "List", TomeTracker.Saga.storyOrder )

end

function TomeTracker.Saga.OnEnterWorld()
	
	--[===[@alpha@
	d("function TomeTracker.Saga.OnEnterWorld()")
	--@end-alpha@]===]
	if firstAddonRun then
		TomeTracker.Saga.InitialEpicEntry()
	end		
end

-- Updating worldPos
function TomeTracker.Saga.OnPositionUpdated()

	-- check for entries needing reward updates
	if TomeTracker.Saga.needRewardUpdate[1] then

		--[===[@alpha@
		d("function TomeTracker.Saga.OnPositionUpdated() - Updating Unlock Item Data")
		--@end-alpha@]===]

		for i, unlockId in ipairs(TomeTracker.Saga.needRewardUpdate) do

			local entryData = TomeTracker.TomeUnlocks[unlockId]
			local section, entry = TomeTracker.Saga.SplitHyperLink( entryData.linkId )

			local rewardInfo

			if (section == GameData.Tome.SECTION_BESTIARY) then
				rewardInfo = TomeTracker.Saga.GetBestiaryRewardInfo(unlockId, entry)
			elseif (section == GameData.Tome.SECTION_ACHIEVEMENTS) then
				local achievementData = TomeGetAchievementsEntryData(tomeAlert.entry)
				rewardInfo = TomeTracker.Saga.GetRewardInfo(achievementData.rewards)
			end

			-- if we have good reward info now replace the existing junk
			if TomeTracker.Saga.VerifyRewardInfo(rewardInfo.reward) then
				entryData.reward = rewardInfo.reward
				-- remove this entry from updates
				TomeTracker.Saga.needRewardUpdate[i] = nil
			end
		end

		TomeTracker.Saga.UpdateEpicSaga()
	end

	-- check for entries needing position updates
	if not TomeTracker.Saga.needPosUpdate[1] then
		return
	end

	--[===[@alpha@
	d("function TomeTracker.Saga.OnPositionUpdated() - Updating Saga Entries World Pos")
	--@end-alpha@]===]

	local playerPos = MapMonsterAPI.GetPlayerPosition()
	local shortPos

	-- Post error if were missing player position of zone position
	if not playerPos or not playerPos.zoneX then 
		-- if its the first error post it
		if not TomeTracker.Saga.havePosError then
			local errorCode, errorString = MapMonsterAPI.GetError()
			TomeTracker.Saga.havePosError = true
			-- if the zone has no map skip the error print out
			if errorCode ~= 104 then
				TomeTracker.print("Zone coordinates error: " .. errorString)
				ERROR(L"(Saga) zonePos - Zone coordinates error: " .. towstring(errorString))
			end
		end
	-- we got everything now get short zone
	else
		-- No reason this should error out but check anyways
		shortPos = MapMonsterAPI.ZoneToShortZone(playerPos)
		if not shortPos then
			if not TomeTracker.Saga.havePosError then
				local errorCode, errorString = MapMonsterAPI.GetError()
				TomeTracker.Saga.havePosError = true
				if errorCode ~= 104 then
					TomeTracker.print("Zone coordinates error: " .. errorString)
					ERROR(L"(Saga) shortPos - Zone coordinates error: " .. towstring(errorString))
				end
			end
		else
			-- Everything went well clear error flag
			TomeTracker.Saga.havePosError = false
		end
	end


	for i, entryId in ipairs(TomeTracker.Saga.needPosUpdate) do

		local locString
		if shortPos then
			locString = LibToolkit.GetZoneName(shortPos.zoneId) .. towstring(" - " .. shortPos.zoneX .. "K, " .. shortPos.zoneY .. "K")
		else
			locString = LibToolkit.GetZoneName(playerPos.zoneId) .. L" - Unknown"
		end
		-- Update player loclString entry
		TomeTracker.Player.Epic[entryId].locString = locString

		-- We only get one shot since the zone pos dont update anymore
		-- so if we have a good Pos use it otherwise give up
		if playerPos.zoneX then
			-- create MapPin for this entry
			TomeTracker.Pins.CreateMapMonsterPin(entryId, playerPos)
		-- If we didnt have a good zone Pos.. save the world pos in the record for later
		elseif playerPos.worldX then
			local errorCode, errorString = MapMonsterAPI.GetError()
			-- if it any other error except no zone map save the position
			if errorCode ~= 104 then
				TomeTracker.Player.Epic[entryId].worldPos = playerPos
			end
		end

		-- Drop entryId from the updates table
		TomeTracker.Saga.needPosUpdate[i] = nil
	end

	TomeTracker.Saga.UpdateEpicSaga()
	
end

function TomeTracker.Saga.ProcessAlert(tomeAlert)

	--[===[@alpha@
	d("function TomeTracker.Saga.ProcessAlert(tomeAlert)")
	--@end-alpha@]===]

	-- Called to process before entering world check for first run
	if firstAddonRun then
		TomeTracker.Saga.InitialEpicEntry()
		UnregisterEventHandler(SystemData.Events.ENTER_WORLD, "TomeTracker.Saga.OnEnterWorld")
	end

	-- Make sure no duplicate alerts sneak in here
	if ( TomeTracker.Player.Epic[tomeAlert.id] ~= nil ) then
		return -- shortcut the processing
	end

	-- Add relevant entries
	TomeTracker.Saga.AddTomeUnlockEntry( tomeAlert )
	TomeTracker.Saga.AddEpicEntry( tomeAlert )
	TomeTracker.Saga.UpdateFullSagaTable( tomeAlert.id )

	-- check player position data
	if (tomeAlert.position == nil or tomeAlert.position == false or tomeAlert.position.zoneId < 1) then
		tomeAlert.position = MapMonsterAPI.GetPlayerPosition()				
	end
	-- Now we add MapMonster Pin
	TomeTracker.Pins.CreateMapMonsterPin(tomeAlert.id, tomeAlert.position)

	table.insert(TomeTracker.Player.Chronology, 1, tomeAlert.id)

	TomeTracker.Saga.UpdateEpicSaga()

end

--
-- Window Event handlers
--

function TomeTracker.Saga.OnMouseOverRow()

	local windowName	= SystemData.ActiveWindow.name
    local parentName	= WindowGetParent(windowName)
    local parentIndex	= WindowGetId(parentName)

	TomeTracker.Saga.HighlightRow(parentName)

	local sagaIndex = TTSagaList.PopulatorIndices[parentIndex]
	local sagaEntry = TomeTracker.Saga.FullSaga[sagaIndex]

	local dateString = LibDateTime:Format(LibDateTime:Parse(tostring(sagaEntry.player.datestamp)), "%Y-%m-%d %H:%M:%S")

	local toolTipText = {}				
	table.insert(toolTipText, {L"Date:", towstring(dateString)})
	table.insert(toolTipText, {L"Zone:", sagaEntry.player.locString})
	table.insert(toolTipText, {L"Level:", towstring(sagaEntry.player.level)})
	table.insert(toolTipText, {L"Renown:", towstring(sagaEntry.player.renown)})

	TomeTracker.Tooltip.CreateTooltip(windowName, toolTipText)
      
end

function TomeTracker.Saga.OnMouseOverReward()

	local windowName	= SystemData.ActiveWindow.name
    local parentName	= WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name))
    local parentIndex	= WindowGetId(parentName)

	TomeTracker.Saga.HighlightRow(parentName)

	local sagaIndex = TTSagaList.PopulatorIndices[parentIndex]

	local toolTipText = {}
	local rewardInfo = TomeTracker.Saga.FullSaga[sagaIndex].tome.reward

	if ( rewardInfo.type == GameData.Tome.REWARD_TITLE ) then
		table.insert(toolTipText, {L"Title:", rewardInfo.label})
	elseif ( rewardInfo.type == GameData.Tome.REWARD_QUEST ) then
		table.insert(toolTipText, {L"Fragment:", rewardInfo.label})
	elseif ( rewardInfo.type == GameData.Tome.REWARD_ABILITY ) then
		table.insert(toolTipText, {L"Ability:", rewardInfo.label})
	elseif ( rewardInfo.type == GameData.Tome.REWARD_ITEM or
			 rewardInfo.type == GameData.Tome.REWARD_ITEM_NO_AUTOCREATE )
	then
		table.insert(toolTipText, {L"Item:", rewardInfo.label})
	elseif ( rewardInfo.type == GameData.Tome.REWARD_ABILITY_COUNTER ) then
		table.insert(toolTipText, {L"Fragment:", rewardInfo.label})
		table.insert(toolTipText, {L"Collected:", rewardInfo.count})
	end

	TomeTracker.Tooltip.CreateTooltip(windowName, toolTipText)
end

function TomeTracker.Saga.OnMouseOverMapPin()

	local windowName	= SystemData.ActiveWindow.name
    local parentName	= WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name))
    local parentIndex	= WindowGetId(parentName)

	TomeTracker.Saga.HighlightRow(parentName)

	local sagaIndex = TTSagaList.PopulatorIndices[parentIndex]
	local sagaEntry = TomeTracker.Saga.FullSaga[sagaIndex]
	local pinData = MapMonsterAPI.GetPin(TomeTracker.Saga.FullSaga[sagaIndex].mapMonsterPin)

	Tooltips.CreateTextOnlyTooltip (windowName, nil)
	Tooltips.SetTooltipText (1, 1,  sagaEntry.player.locString)
	Tooltips.Finalize()

    local anchor = { Point = "bottomright",
   					RelativeTo = "CursorWindow",
   					RelativePoint = "topleft",
   					XOffset = -50,
   					YOffset = -25 }

    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
	
end

function TomeTracker.Saga.OnMouseOverRowEnd()
	local parentName = WindowGetParent(SystemData.ActiveWindow.name)
	TomeTracker.Saga.NormalRow(parentName)
end

function TomeTracker.Saga.OnMouseOverRewardEnd()
	local parentName = WindowGetParent(WindowGetParent(SystemData.ActiveWindow.name))
	TomeTracker.Saga.NormalRow(parentName)
end

function TomeTracker.Saga.OnMouseOverMapPinEnd()
	TomeTracker.Saga.OnMouseOverRewardEnd()
end

function TomeTracker.Saga.OpenTome()
	--[===[@alpha@
	d("TomeTracker.Saga.OpenTome()")
	--@end-alpha@]===]
	local windowName	= SystemData.ActiveWindow.name
	local windowIndex	= WindowGetId(windowName)
	
	local tomeLink = TomeTracker.Saga.FullSaga[TTSagaList.PopulatorIndices[windowIndex]].tome.linkId

	TomeTracker.Saga.OnHyperLinkClicked(tomeLink)

end

function TomeTracker.Saga.HighlightMapPin()

	local windowName	= SystemData.ActiveWindow.name
    local mapPinId	    = WindowGetId(windowName)

	if mapPinId and mapPinId > 0 then
		MapMonsterAPI.HighlightMapPin(mapPinId)
	end
	
end

-- Sets the background tinting for each row in the saga list
function TomeTracker.Saga.SetListRowTints()

    for rowNum = 1, TTSagaList.numVisibleRows do
		local targetRowWindow = TomeTracker.Saga.windowName .. "ListRow"..rowNum
		TomeTracker.Saga.NormalRow(targetRowWindow)
    end
end


function TomeTracker.Saga.HighlightRow(rowWindowName)

	local rowBackground = rowWindowName .. "RowBackground"

    WindowSetTintColor(rowBackground, 255, 255, 255 )
    WindowSetAlpha(rowBackground, 0.1)

end

function TomeTracker.Saga.NormalRow(rowWindowName)

	local rowBackground = rowWindowName .. "RowBackground"

    WindowSetTintColor(rowBackground, 0, 0, 0 )
    WindowSetAlpha(rowBackground, 0)

end

--
-- Saga List Functions
--

-- Callback from the <list> function
function TomeTracker.Saga.Populate()

    if (nil == TTSagaList.PopulatorIndices ) then
		return
    end

	for rowNum, entryId in ipairs(TTSagaList.PopulatorIndices) do

		local rowFrame   = TomeTracker.Saga.windowName .. "ListRow"..rowNum
		local entryData = TomeTracker.Saga.FullSaga[entryId]

		if entryData ~= nil then

		-- section icon display
			if (entryData.tome.section == nil) then
				WindowSetShowing(rowFrame .. "EntryIcon", false)
			elseif (entryData.tome.section == 0) then -- RvR Icon
				DynamicImageSetTexture(rowFrame .. "EntryIcon", "EA_HUD_01", 0, 0)
				DynamicImageSetTextureScale(rowFrame .. "EntryIcon", 0.5)
				DynamicImageSetTextureSlice(rowFrame .. "EntryIcon", "RvR-Flag")

				WindowSetShowing(rowFrame .. "EntryIcon", true)
			elseif (entryData.tome.section == 26) then -- Tactic icon
				local texture, x, y = GetIconData( entryData.tome.iconNum )

				DynamicImageSetTexture(rowFrame .. "EntryIcon", texture, x, y)
				DynamicImageSetTextureScale(rowFrame .. "EntryIcon", 0.34)

				WindowSetShowing(rowFrame .. "EntryIcon", true)								
			else -- Basic section icon
				local icon = DataUtils.GetTomeSectionIcon(entryData.tome.section, false)

				DynamicImageSetTexture(rowFrame .. "EntryIcon", "EA_Tome", 0,0)
				DynamicImageSetTextureScale(rowFrame .. "EntryIcon", 0.9)
				DynamicImageSetTextureSlice(rowFrame .. "EntryIcon", icon)

				WindowSetShowing(rowFrame .. "EntryIcon", true)
			end

			-- reward icon display
			if (entryData.tome.reward == nil or entryData.tome.reward.iconNum == nil) then
				WindowSetShowing(rowFrame .. "EntryReward", false)
			else
				local texture, x, y = GetIconData( entryData.tome.reward.iconNum )
    		    DynamicImageSetTexture( rowFrame .. "EntryReward", texture, x, y )
        		DynamicImageSetTextureScale(rowFrame .. "EntryReward", 0.35)

				WindowSetShowing(rowFrame .. "EntryReward", true)
			end

			-- mappin icon display
			if (entryData.player.mapMonsterPin) then
				DynamicImageSetTexture( rowFrame .. "EntryPin", "map_markers01", 0, 0)
				DynamicImageSetTextureScale(rowFrame .. "EntryPin", 0.7)
				DynamicImageSetTextureSlice(rowFrame .. "EntryPin", "QuestActive")

				WindowSetShowing(rowFrame .. "EntryPin", true)
				WindowSetId(rowFrame .. "EntryPin", entryData.player.mapMonsterPin)
			else
				WindowSetShowing(rowFrame .. "EntryPin", false)
				WindowSetId(rowFrame .. "EntryPin", 0)
			end

		end
	end
end

function TomeTracker.Saga.BuildFullSagaTable()

	--[===[@alpha@
	d("function TomeTracker.Saga.BuildFullSagaTable()")
	--@end-alpha@]===]
	local playerSaga = TomeTracker.Player.Epic
	local tomeUnlocks = TomeTracker.TomeUnlocks
	local fullSagaTable = {}

	-- this is building a simple table with references to shared unlock data and player specific data
	for unlockId, unlockData in pairs(playerSaga) do
		fullSagaTable[unlockId] = {
									id = unlockData.id,
									label = tomeUnlocks[unlockId].label,
									isShared = tomeUnlocks[unlockId].isShared,
									tome = tomeUnlocks[unlockId],
									player = unlockData,
									}
		-- special case we want to use the players label not the shared label
		if (unlockId == 24001) then
			fullSagaTable[unlockId].label = unlockData.label
		end

	end

	TomeTracker.Saga.FullSaga = fullSagaTable

end

function TomeTracker.Saga.FilterEpicSaga()

	--[===[@alpha@
	d("function TomeTracker.Saga.FilterEpicSaga()")
	--@end-alpha@]===]
	TomeTracker.Saga.storyOrder = {}

	-- no actual filtering or sorting happens here yet

	for key, data in pairs( TomeTracker.Player.Chronology ) do
		table.insert(TomeTracker.Saga.storyOrder, data)
	end

end

--
-- Internal functions
--

function TomeTracker.Saga.CheckForFirstRun()
	
	--[===[@alpha@
	d("function TomeTracker.Saga.CheckForFirstRun()")
	--@end-alpha@]===]

	-- if we have the first line we've Run with the addon before
	if TomeTracker.Player.Epic[24001] then
		firstAddonRun = false
	end

end


function TomeTracker.Saga.InitialEpicEntry()

	--[===[@alpha@
	d("function TomeTracker.Saga.InitialEpicEntry()")
	--@end-alpha@]===]
	-- shortcut if this isnt the first Run anymore
	if not firstAddonRun then
		return
	end

	firstAddonRun = false

	-- Add the first entry to the players saga
	local initialEntry = {
			-- simulate Tome Alert values
			-- internal tome alert ids are from 1 to 12k - so I start at 24k
			id = 24001, -- internal unique id per alert/unlock
			section = 0, -- tome section
			entry = 0, -- tome entry type
			subEntry = 0,
			name = L"The heroic tale began in " .. LibToolkit.GetZoneName(GameData.Player.zone) .. L" at level " .. GameData.Player.level,
			desc = L"We joined the epic saga of our hero, " .. GameData.Player.name .. L", at level " .. GameData.Player.level .. L" in " .. LibToolkit.GetZoneName(GameData.Player.zone),
			rewardId = 0,
			rewardType = 0,
			isShared = false,
	}		

	local initialEntry = TomeTracker.Journal.AssembleAlertData( initialEntry )

	-- if were squeezing this in before a new entry... make sure the timestamp on this one is first
	if ( TomeTracker.Journal.PendingAlerts[1] ~= nil ) then
		initialEntry.timestamp = TomeTracker.Journal.PendingAlerts[1].timestamp
	end				

	-- Send this entry through regular processing
	TomeTracker.Saga.ProcessAlert(initialEntry)

end

function TomeTracker.Saga.AddTomeUnlockEntry(tomeAlert)

	--[===[@alpha@
	d("function TomeTracker.Saga.AddTomeUnlockEntry(tomeAlert)")
	--@end-alpha@]===]

	-- entry doesnt exists, Create it
	if not TomeTracker.TomeUnlocks[tomeAlert.id] then
		local newUnlockEntry = TomeTracker.Saga.BuildTomeUnlockEntry(tomeAlert)
		TomeTracker.TomeUnlocks[tomeAlert.id] = newUnlockEntry
	end

end

function TomeTracker.Saga.BuildTomeUnlockEntry(tomeAlert)

	--[===[@alpha@
	d("function TomeTracker.Saga.BuildTomeUnlockEntry(tomeAlert)")
	--@end-alpha@]===]

	local newUnlockEntry = {
					id       = tomeAlert.id,
					linkId   = tomeAlert.linkId,
					label    = tomeAlert.name,
					section  = tomeAlert.section,
					isShared = tomeAlert.isShared,
					MapMonsterPins = {},
					}

	local entryData

	-- gather reward info and descriptive labels based on tome section		
	if (tomeAlert.section == GameData.Tome.SECTION_BESTIARY) then
		entryData = nil

		local bestiaryInfo = TomeTracker.Saga.GetBestiaryRewardInfo(tomeAlert.id, tomeAlert.entry)

		-- if this entry even has rewards to post
		if ( bestiaryInfo.reward ~= nil ) then
			-- check we have all the info we need for this reward
			local goodReward = TomeTracker.Saga.VerifyRewardInfo(bestiaryInfo.reward)
			if not goodReward then
				-- add this entry to the rewardsUpdate queue
				table.insert(TomeTracker.Saga.needRewardUpdate, tomeAlert.id)
			end
			newUnlockEntry.reward = bestiaryInfo.reward
		end

		newUnlockEntry.cardId = bestiaryInfo.cardId

		local labelString = WStringToString(tomeAlert.name)

		-- so many Victim unlocks let's be more descriptive
		if ( string.match(labelString, "^Victim") ) then
			local _, _, label = string.find(WStringToString(tomeAlert.desc), "^You have completed: (.-)$")
			newUnlockEntry.label = towstring(label)
		end

	elseif (tomeAlert.section == GameData.Tome.SECTION_ACHIEVEMENTS) then
		--entryData = TomeGetAchievementsEntryData(tomeAlert.entry)
		entryData = nil

		local achievementData = TomeGetAchievementsEntryData(tomeAlert.entry)
		local rewardInfo = TomeTracker.Saga.GetRewardInfo(achievementData.rewards)

		if rewardInfo then
			-- check we have all the info we wanted
			local goodReward = TomeTracker.Saga.VerifyRewardInfo(rewardInfo)
			if not goodReward then
				-- add this entry to the rewardsUpdate queue
				table.insert(TomeTracker.Saga.needRewardUpdate, tomeAlert.id)
			end
			newUnlockEntry.reward = rewardInfo
		end

		-- add card id for later since I have none to test with we need to keep it
		if (achievementData.cardId ~= 0) then
			newUnlockEntry.cardId = achievementData.cardId
		end								

	elseif (tomeAlert.section == GameData.Tome.SECTION_NOTEWORTHY_PERSONS) then
		--entryData = TomeGetNoteworthyPersonsEntryData(tomeAlert.entry)
		-- has no rewards or extra info
		entryData = nil

	elseif (tomeAlert.section == GameData.Tome.SECTION_HISTORY_AND_LORE) then
		--entryData = TomeGetHistoryAndLoreEntryData(tomeAlert.entry)
		-- has no rewards or extra info
		entryData = nil

	elseif (tomeAlert.section == GameData.Tome.SECTION_OLD_WORLD_ARMORY) then
		-- TODO: need to research this more
		-- seems the first time you Run this cmd you get an almost empty list
		-- like the item cache hasnt caught up yet, any subsequent calls work fine
		-- as expected, seems to be the same for item rewards and possibly tactics

		-- has no rewards or extra info we want to use
		--entryData = TomeGetOldWorldArmoryArmorSet(tomeAlert.entry)
		entryData = nil

		local _, _, label = string.find(WStringToString(tomeAlert.desc), "^The (.-) had been added to your Armory!$")
		newUnlockEntry.label = towstring(label)

	elseif (tomeAlert.section == GameData.Tome.SECTION_WAR_JOURNAL) then
		--local journalData = TomeGetWarJournalEntryData(tomeAlert.entry)
		-- has no rewards or extra info
		entryData = nil

		-- Chapters, Battlefield Objectives, PQs and PQ stages all look the same
		-- parse the alert name and desc to be more descriptive
		local jStrings = {}
		local newLabel = ""

		_, _, jStrings.title, jStrings.name = string.find(WStringToString(tomeAlert.name), "^(.-): (.-)$")
		_, _, jStrings.pqQuest = string.find(WStringToString(tomeAlert.desc), "^You have discovered the Public Quest: (.-)$")
		_, _, jStrings.pqStage = string.find(WStringToString(tomeAlert.desc), "^You have completed PQ Stage: (.-)$")
		_, _, jStrings.BOString = string.find(WStringToString(tomeAlert.desc), "^You have discovered the Battlefield Objective: (.-)$")

		if (jStrings.BOString ~= nil) then
			newLabel = "Battlefield Objective: " .. jStrings.BOString
		elseif (jStrings.pqStage ~= nil) then
			if (jStrings.name == nil) then jStrings.name = "" end
			newLabel = jStrings.name .. " PQ: " .. jStrings.pqStage
		elseif (jStrings.pqQuest ~= nil) then
			if (jStrings.title == nil) then jStrings.title = "" end
			newLabel = jStrings.title .. ": " .. jStrings.pqQuest .. " PQ"
		end

		if (newLabel ~= "") then
			newUnlockEntry.label = towstring(newLabel)
		end
	elseif (tomeAlert.section == GameData.Tome.SECTION_PLAYER_TITLES) then
		--entryData = TomeGetPlayerTitleData(tomeAlert.entry)
		-- has no rewards or extra info we want
		entryData = nil

	elseif (tomeAlert.section == GameData.Tome.SECTION_TACTICS) then
		-- TODO: need to research this more, not sure what info we have access to
		--entryData = TomeGetTacticCounterData(tomeAlert.entry)
		local tacticsList = TomeGetTacticRewardsList()

		for i, tacticData in ipairs(tacticsList) do
			if (tacticData.abilityId == tomeAlert.entry) then
				newUnlockEntry.label = tacticData.name
				newUnlockEntry.iconNum = tacticData.iconNum
				break -- skip the rest we found it
			end
		end

	elseif (tomeAlert.section == GameData.Tome.SECTION_LIVE_EVENT) then
		-- TODO: need to research this more, not sure what info we have access to
		entryData = GetLiveEventData()

	end

	-- stuff any unknown data into a field for later review
	newUnlockEntry.tomeData = entryData

	return newUnlockEntry

end

function TomeTracker.Saga.GetBestiaryRewardInfo(alertId, entryId)

	--[===[@alpha@
	d("function TomeTracker.Saga.GetBestiaryRewardInfo(alertId, entryId)")
	--@end-alpha@]===]

	local bestiaryRewards = {}
	local speciesData = TomeGetBestiarySpeciesData(entryId)
	-- find the task we just unlocked
	for i, taskData in pairs(speciesData.tasks) do
		if (taskData.unlockEventId == alertId) then
			local rewardInfo = TomeTracker.Saga.GetRewardInfo(taskData.rewards)

			if rewardInfo then
				bestiaryRewards.reward = rewardInfo
			end

			-- add card id for later since I have none to test with we need to keep it
			if (taskData.cardId ~= 0) then
				bestiaryRewards.cardId = taskData.cardId
			end
			break -- skip the rest since we've found it
		end
	end
	return bestiaryRewards

end

function TomeTracker.Saga.GetRewardInfo(rewards)

	--[===[@alpha@
	d("function TomeTracker.Saga.GetRewardInfo(rewards)")
	--@end-alpha@]===]

	local rewardInfo = false

	for rewardIdx, rewardData in pairs(rewards) do
		-- skip xp rewards and empty rewardtypes
		if (rewardData.rewardId ~= 0 and rewardData.rewardType ~= 0) then

			rewardSlot = { id = rewardData.rewardId,
						   type = rewardData.rewardType, }

		   --Set up the icon for the reward

      		if( rewardData.rewardType == GameData.Tome.REWARD_TITLE ) then
   				local titleData = TomeGetPlayerTitleData( rewardData.rewardId )

				rewardSlot.label = titleData.name
	            rewardSlot.iconNum = GameDefs.Icons.ICON_TITLE_REWARD

			elseif( rewardData.rewardType == GameData.Tome.REWARD_QUEST ) then
           		-- TODO: need to research this more, not sure what info we have access to

       		elseif( rewardData.rewardType == GameData.Tome.REWARD_ABILITY ) then
   				-- TODO: need to research this more, not sure what info we have access to
   				-- dont know any unlocks that reward an ability directly
			    local tacticData = TomeGetTacticRewardData( rewardData.rewardId )

				rewardSlot.label = tacticData.name
				rewardSlot.iconNum = tacticData.iconNum
				rewardSlot.tacticData = tacticData

       		elseif( rewardData.rewardType == GameData.Tome.REWARD_ITEM or
          		    rewardData.rewardType == GameData.Tome.REWARD_ITEM_NO_AUTOCREATE )
       		then
   				-- TODO: need to research this more, not sure what info we have access to
   				-- fixed for now, we delay the update of item info
				local itemData = TomeGetItemRewardData( rewardData.rewardId )

				rewardSlot.label = itemData.name
           		rewardSlot.iconNum = itemData.iconNum
           		rewardSlot.itemData = itemData

       		elseif( rewardData.rewardType == GameData.Tome.REWARD_ABILITY_COUNTER ) then
   				--newUnlockEntry.tacticCounter = TomeGetTacticCounterName(rewardData.rewardId)
				--local label, count, tacticsList = TomeGetTacticCounter(rewardData.rewardId)
				local label, count = TomeGetTacticCounter(rewardData.rewardId)

				rewardSlot.label = label
				rewardSlot.iconNum = GameDefs.Icons.ICON_TACTIC_REWARD
				rewardSlot.count = count
			end

			-- add the reward info to the table
			rewardInfo = rewardSlot

		end
	end

	return rewardInfo

end

function TomeTracker.Saga.VerifyRewardInfo(rewardInfo)

		--[===[@alpha@
		d("function TomeTracker.Saga.VerifyRewardInfo(rewardInfo)")
		--@end-alpha@]===]

		if not rewardInfo then
				return false
		end

		if (rewardInfo.type == GameData.Tome.REWARD_TITLE or
		    rewardInfo.type == GameData.Tome.REWARD_QUEST or
		    rewardInfo.type == GameData.Tome.REWARD_ABILITY or
		    rewardInfo.type == GameData.Tome.REWARD_ITEM or
		    rewardInfo.type == GameData.Tome.REWARD_ITEM_NO_AUTOCREATE )
		then
				if (rewardInfo.id      == nil or type(rewardInfo.id)      ~= "number" or rewardInfo.id      < 1    or
						rewardInfo.label   == nil or type(rewardInfo.label)  ~= "wstring" or rewardInfo.label   == L"" or
						rewardInfo.iconNum == nil or type(rewardInfo.iconNum) ~= "number" or rewardInfo.iconNum < 1 )
				then
						-- Something missing or bad value - Failed!
						return false
				end
		elseif (rewardInfo.type == GameData.Tome.REWARD_ABILITY_COUNTER) then
				if (rewardInfo.id      == nil or type(rewardInfo.id)      ~= "number" or rewardInfo.id      < 1    or
						rewardInfo.label   == nil or type(rewardInfo.label)  ~= "wstring" or rewardInfo.label   == L"" or
						rewardInfo.iconNum == nil or type(rewardInfo.iconNum) ~= "number" or rewardInfo.iconNum < 1    or
						rewardInfo.count   == nil or type(rewardInfo.count)   ~= "number" or rewardInfo.count   < 1 )
				then
						-- Something missing or bad value - Failed!
						return false
				end
		else
				-- missed all the reward types something wrong here
				return false
		end

		return true
end


function TomeTracker.Saga.AddEpicEntry(tomeAlert)

	--[===[@alpha@
	d("function TomeTracker.Saga.AddEpicEntry(tomeAlert)")
	--@end-alpha@]===]

	local newEntry = {
					id = tomeAlert.id,
					level = tomeAlert.level,
					renown = tomeAlert.renown,
					datestamp = tonumber(LibDateTime:Format(tomeAlert.timestamp, "%Y%m%d%H%M%S")),
					isShared = tomeAlert.isShared, -- temporary duplication should be only in unlock entry
					}

	local locString = L""

	-- Generate Location String from Position
	if tomeAlert.position then
		local shortPos = MapMonsterAPI.ZoneToShortZone(tomeAlert.position)
		if shortPos then
			locString = LibToolkit.GetZoneName(shortPos.zoneId) .. towstring(" - " .. shortPos.zoneX .. "K, " .. shortPos.zoneY .. "K")
		else
			if (tomeAlert.position.zoneId > 0) then
				locString = LibToolkit.GetZoneName(tomeAlert.position.zoneId) .. L" - "
			end
			locString = locString .. L"Unknown"
		end
	else
		locString = L"Unknown"
	end
	newEntry.locString = locString
	
	-- add extra field for the start of the epic
	if (newEntry.id == 24001) then
		newEntry.label = tomeAlert.name
	end
	TomeTracker.Player.Epic[newEntry.id] = newEntry

end

function TomeTracker.Saga.UpdateFullSagaTable(alertId)

	--[===[@alpha@
	d("function TomeTracker.Saga.UpdateFullSagaTable(alertId)")
	--@end-alpha@]===]
	local player = TomeTracker.Player.Epic[alertId]
	local common = TomeTracker.TomeUnlocks[alertId]

	TomeTracker.Saga.FullSaga[alertId] = {
										id = alertId,
										label = common.label,
										isShared = common.isShared,
										tome = common,
										player = player,
										}

	if (alertId == 24001) then
		TomeTracker.Saga.FullSaga[alertId].label = player.label
	end

end


function TomeTracker.Saga.SavePin(entryId, pinPos)

	MapMonsterAPI.CreatePin(pinType, subType, label, pinPos, note, addonData, merge)

end

function TomeTracker.Saga.AddPinToUnlock(alertId, position)

	if not TomeTracker.TomeUnlocks[alertId].position then
		TomeTracker.TomeUnlocks[alertId].position = {}
	end

end

function TomeTracker.Saga.GetTomeUnlockPosition(entryId)

		--[===[@alpha@
		d("function TomeTracker.Saga.GetTomeUnlockPosition(entryId)")
		--@end-alpha@]===]

		-- for now just grab the last entry
		local lastEntry = #TomeTracker.TomeUnlocks[entryId].position[GameData.Player.realm][entryPos.zoneId]
		local entryPos = TomeTracker.TomeUnlocks[entryId].position[GameData.Player.realm][entryPos.zoneId][lastEntry]

		return entryPos
		
end

--
-- Utility functions
--


function TomeTracker.Saga.OnHyperLinkClicked( linkParam )

		local section, entry, subentry = TomeTracker.Saga.SplitHyperLink( linkParam )
  
  	-- a little broken since they forgot to add a var in TomeWindow
  	if (section == 26) then
  			section = 25
  	end

    TomeWindow.OpenTomeToEntry( section, entry, subEntry )
    
end

function TomeTracker.Saga.SplitHyperLink( linkParam )

    -- Tome Links should be in this format: L"TOME1:200"
    -- This means section 1, entry 200
     -- or
    -- Tome Links should be in this format: L"TOME20:2:10"
    -- This means section 20, entry 2, subEntry 10
    
    if( wstring.sub(linkParam, 1, 4) ~= L"TOME" ) then
       return
    end
    
    local colonPos1  = wstring.find(linkParam, L":", 1)
       
    local section   = tonumber( wstring.sub(linkParam, 5, colonPos1-1) )
   
    local subString = wstring.sub(linkParam, colonPos1+1 )
    local colonPos2  = wstring.find(subString, L":")
    
    local entry, subEntry
    if( colonPos2 ~= nil ) then     
        entry       = tonumber( wstring.sub(subString, 0, colonPos2-1 ) )
        subEntry    = tonumber( wstring.sub(subString, colonPos2+1 ) )
    else
        entry       = tonumber( subString )
        subEntry    = 0
    end
  
		return section, entry, subEntry
		    
end

--
-- Testing and debugging functions
--

function TomeTracker.Saga.ReprocessAlert(alertId)

		-- Make sure this is deliberate
		if not TomeTracker.Saga.isBeta then
				return
		end
		
		--[===[@alpha@
		d("**** TomeTracker.Saga.ReprocessAlert(alertId) - Reprocessing alert # " .. alertId)
		--@end-alpha@]===]
		
		local tomeAlerts = LibToolkit.CopyObject(DataUtils.GetTomeAlerts())
		local repeatAlert = false
		
		-- find the alert data we want to reprocess
		for i, alertData in pairs(tomeAlerts) do
				if (alertData.id == alertId) then
						repeatAlert = alertData
						break -- found the alert we want skip the rest of the list
				end
		end

		-- didnt find the alert we wanted in the tome list
		if not repeatAlert then
				ERROR("***** Alert " .. alertId .. " not found in Tome of Knowledge New Entries list cannot reprocess")
				return
		end
		
		-- repackage the alert
		local newAlert = TomeTracker.Journal.AssembleAlertData( repeatAlert )
		
		-- first we nuke the current table entries
		TomeTracker.Saga.FullSaga[newAlert.id] = nil
		TomeTracker.Player.Epic[newAlert.id] = nil
		TomeTracker.TomeUnlocks[newAlert.id] = nil
		
		local chronoIdx = false
		for i, entry in ipairs(TomeTracker.Player.Chronology) do
				if (newAlert.id == entry) then
						chronoIdx = i
				end
		end
		
		if chronoIdx then
				table.remove(TomeTracker.Player.Chronology, chronoIdx)
		end
		
		-- send it along the chain again
		TomeTracker.Saga.ProcessAlert(newAlert)

end

function TomeTracker.Saga.DeleteEntry(alertId)

		-- Make sure this is deliberate
		if not TomeTracker.Saga.isBeta then
				return
		end

		TomeTracker.print("Deleted alert #" .. alertId .. " from TomeUnlocks Table")		
		TomeTracker.TomeUnlocks[alertId] = nil

end