--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Debug.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------
if not TomeTracker then return end -- shortcut file loading if theres a problem with the core file

local LibToolkit = LibStub("LibToolkit-0.1")

TomeTracker_Debug = {}
TomeTracker_Debug.OtherEvents = {
		"LOADING_BEGIN",
		"LOADING_END",
		"INTERFACE_RELOADED",
		"RELOAD_INTERFACE",
		"ENTER_WORLD",
		"SCENARIO_BEGIN",
		"SCENARIO_END",
		"PLAYER_EXP_UPDATED",
		"PLAYER_EXP_TABLE_UPDATED",
		"PLAYER_CAREER_RANK_UPDATED",
		"PLAYER_RENOWN_UPDATED",
		"PLAYER_ZONE_CHANGED",
}

function TomeTracker_Debug.Initialize()

		d("Initializing TomeTracker Debug")
		-- Register most event for troubleshooting
		--TomeTracker_Debug.RegisterEvents()
		TomeTracker_Debug.registerTomeEvents()
		--TomeTracker_Debug.FloodDebug()

		TomeTracker.print("Debug functions Loaded")

end

function TomeTracker_Debug.registerTomeEvents()
		--TomeTracker_Debug.EventDebug_AllWithString("TOME")
		TomeTracker_Debug.EventDebug_AllWithString("MAP")
		TomeTracker_Debug.EventDebug_AllWithString("QUIT")
		TomeTracker_Debug.EventDebug_AllWithString("RELOAD")
		
		for i, event in ipairs(TomeTracker_Debug.OtherEvents) do
				_G["TomeTracker_Debug_" .. event] = function(...)
						d(event .. ": " .. table.concat({...}, ", "))
						end
				RegisterEventHandler(SystemData.Events[event], "TomeTracker_Debug_" .. event)	
		end
end

function TomeTracker_Debug.EventDebug_AllWithString(t)
	for event in pairs(SystemData.Events) do -- iterate through, getting the name of all events
		if string.find(event, t) then
			_G["TomeTracker_Debug_" .. event] = function(...)
					d(event .. ": " .. table.concat({...}, ", "))
					end
			RegisterEventHandler(SystemData.Events[event], "TomeTracker_Debug_" .. event)
		end
	end
end

function TomeTracker_Debug.RegisterEvents()

	for event in pairs(SystemData.Events) do

		if string.find(event, "TOME") or string.find(event, "GUILD_")
			or string.find(event, "CHAT_")  or string.find(event, "PLAYER_")
			or string.find(event, "GROUP_") or string.find(event, "CAMPAIGN_")
			or string.find(event, "ENTER_KEY_")
			or string.find(event, "L_BUTTON") or string.find(event, "R_BUTTON")  then
			-- skip event Register
		else
			_G["EventDebug_" .. event] = function(...)
					d(event .. ": " .. table.concat({...}, ", "))
					end
			RegisterEventHandler(SystemData.Events[event], "EventDebug_" .. event)
		end

	end

	--    DO NOT TURN THIS ON IT TRIGGERS ON ANYTHING, EVEN ITSELF
	UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "EventDebug_UPDATE_PROCESSED")

	--    Well documented, and causes alot of spam during normal operation and movement
	UnregisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "EventDebug_PLAYER_POSITION_UPDATED")

	UnregisterEventHandler(SystemData.Events.PLAYER_INFLUENCE_UPDATED, "EventDebug_PLAYER_INFLUENCE_UPDATED")
	UnregisterEventHandler(SystemData.Events.PLAYER_INFLUENCE_REWARDS_UPDATED, "EventDebug_PLAYER_INFLUENCE_REWARDS_UPDATED")

end

function TomeTracker_Debug.FloodDebug()

		RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "TomeTracker_Debug.UpdateProcessed")

end

function TomeTracker_Debug.UpdateProcessed()
		d(L"## Character: " .. GameData.Player.name .. L" Level: " .. GameData.Player.level .. L" ZoneID: " .. GameData.Player.zone)
end


function TomeTracker_Debug.GetZoneList()

		TomeTracker_Data.ZoneList = {}

		for i = 1, 350 do
			local zoneName = LibToolkit.GetZoneName(i)
			if (zoneName ~= L"") then
					TomeTracker_Data.ZoneList[i] = zoneName
			end
		end
		InterfaceCore.ReloadUI()
		d("Saved ZoneName List to Saved Variables")
end

function TomeTracker_Debug.DelZoneList()
		TomeTracker_Data.ZoneList = nil
		d("Zone List Deleted from Saved Variables")
end

function TomeTracker_Debug.Dump(thing, prefix)

		if not prefix then 
				prefix = ""
		end 
		
		for k,v in pairs(thing) do 
--				EA_ChatWindow.Print(towstring(prefix..k.." ["..type(v).."]"))
				d(towstring(prefix..k.." ["..type(v).."]"))
				if type(v)=="table" then
						TomeTracker_Debug.Dump(v, "    "..prefix)
				end
		end
		
end

function TomeTracker_Debug.HighlightWindow( window )
 	if( WindowGetShowing( HelpTips.FOCUS_WINDOW_NAME ) ) then
 		WindowStopAlphaAnimation( HelpTips.FOCUS_WINDOW_NAME )
 		WindowSetShowing( HelpTips.FOCUS_WINDOW_NAME, false )
 	end
 	HelpTips.SetFocusOnWindow( window )
end

function TomeTracker_Debug.CycleZoneStart()
		TomeTracker_Debug.lastZone = 0
		RegisterEventHandler(SystemData.Events.ENTER_KEY_PROCESSED, "TomeTracker_Debug.CycleZones")
end

function TomeTracker_Debug.CycleZones()
		TomeTracker_Debug.lastZone = TomeTracker_Debug.lastZone + 1
		EA_Window_WorldMap.SetMap(GameDefs.MapLevel.ZONE_MAP, TomeTracker_Debug.lastZone)
		d("Switch to map: ( "..TomeTracker_Debug.lastZone.." ) " .. LibToolkit.GetZoneName(TomeTracker_Debug.lastZone) )
end

function TomeTracker_Debug.CycleZoneReset()
		TomeTracker_Debug.lastZone = 0
		--UnregisterEventHandler(SystemData.Events.ENTER_KEY_PROCESSED, "TomeTracker_Debug.CycleZones")
end


function TomeTracker_Debug.Coords(windowName)

				if (windowName == nil) then
						windowName = SystemData.MouseOverWindow.name
				end
        local mapPositionX, mapPositionY = WindowGetScreenPosition( windowName )
        local x, y = MapGetCoordinatesForPoint(windowName,
                                               SystemData.MousePosition.x - mapPositionX,
                                               SystemData.MousePosition.y - mapPositionY)
			if x == nil then x = "" end
			if y == nil then y = "" end

			d(windowName.." x: "..x.." y: "..y)

end

function TomeTracker_Debug.CornerIcon(icon)

    local texture, x, y = GetIconData( icon )
    DynamicImageSetTexture( TomeTracker.Journal.windowName .. "CornerImage", icon, x, y )
		d("Switch to icon: ( ".. texture .." ) ")
		
end

function TomeTracker_Debug.CornerTexture(texture)

    DynamicImageSetTexture( TomeTracker.Journal.windowName .. "CornerImage", texture, 0, 0 )
		
		d("Switch to texture: ( ".. texture .." ) ")

end

function TomeTracker_Debug.CycleCornerStart()
		TomeTracker_Debug.lastCorner = 0
		RegisterEventHandler(SystemData.Events.ENTER_KEY_PROCESSED, "TomeTracker_Debug.CycleCorner")
		
		TomeTracker_Debug.CornerImages = {
				"EA_CornerIcon_Apothecary",
				"EA_CornerIcon_Aucton",
				"EA_CornerIcon_Bag",
				"EA_CornerIcon_Character",
				"EA_CornerIcon_Cultivating",
				"EA_CornerIcon_CustomizeUI",
				"EA_CornerIcon_Guild",
				"EA_CornerIcon_Help",
				"EA_CornerIcon_Mail",
				"EA_CornerIcon_Merchant",
				"EA_CornerIcon_Mount",
				"EA_CornerIcon_Quest",
				"EA_CornerIcon_RvR",
				"EA_CornerIcon_Settings",
				"EA_CornerIcon_Spellbook",
				"EA_CornerIcon_Talisman",
				"EA_CornerIcon_Travel",
				"EA_CornerIcon_Social",
		}
		
end

function TomeTracker_Debug.CycleCornerStop()
		TomeTracker_Debug.lastCorner = 0
		UnregisterEventHandler(SystemData.Events.ENTER_KEY_PROCESSED, "TomeTracker_Debug.CycleCorner")
end

function TomeTracker_Debug.CycleCorner()
		TomeTracker_Debug.lastCorner = TomeTracker_Debug.lastCorner + 1
		
		if (TomeTracker_Debug.CornerImages[TomeTracker_Debug.lastCorner] == nil) then
				TomeTracker_Debug.lastCorner = 1
		end
		
		TomeTracker_Debug.CornerTexture(TomeTracker_Debug.CornerImages[TomeTracker_Debug.lastCorner])
		
end

function TomeTracker_Debug.CycleCornerReset()
		TomeTracker_Debug.lastZone = 0
end


function TomeTracker_Debug.TestCopies()

			local firstTable = {first = 1, second = 2, third = 3}
			d("First Table")
			d(firstTable)
			
			-- this is reference to the same table as firstTable
			local secondTable = firstTable

			firstTable.first = 8

			d("Second Table - After firstTable.first = 8 ")
			d(secondTable)

			-- this is an entirely new table with its own values copied from the first table
			secondTable = {}
			secondTable.first = firstTable.first
			secondTable.second = firstTable.second
			secondTable.third = firstTable.third
			d("Created new secondTable with copy of values from firstTable")

			-- this only affects the first table
			firstTable.third = 99

			d("Second Table - After firstTable.third = 99 ")
			d(secondTable)

			-- this only affects the second table
			secondTable.second = "BOO!"
			
			-- this is a table of references to the original tables
			local allTables = {firstTable = firstTable, secondTable = secondTable}
			d("All Tables - After secondTable.second = BOO!")
			d(allTables)
			
			-- this is a reference to the table in allTables which reference the original tables
			local thirdTable = allTables.secondTable
			d("Third Table")
			d(thirdTable)
			
			secondTable.first = "Surprise!"

			d("All Tables - After secondTable.first = Surprise!")
			d(allTables)

			d("Third Table - After secondTable.first = Surprise!")
			d(thirdTable)

			thirdTable.third = "Changed by third"
			
			d("Second Table - After thirdTable.third = Changed by third")
			d(secondTable)
			
			local var1 = allTables.secondTable.second
			var1 = "another change" 
			d(allTables)

			d("-----------------------------")
			
			oneString = firstTable.first
			d(oneString)
			
			firstTable.first = "changed by first"
			
			d(oneString)
			d(firstTable)
			
			secondString = oneString
			d("firstString")
			d(oneString)
			d("secondString")
			d(secondString)

			oneString = oneString + 1000
			
			d("firstString")
			d(oneString)
			d("secondString")
			d(secondString)

			d("-----------------------------")

			aVal = 1000
			bVal = aVal
			
			d("aVal = " .. aVal)
			d("bVal = " .. bVal)
			
			aVal = "Midgets"
			
			d("aVal = " .. aVal)
			d("bVal = " .. bVal)
			
			
end
