--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Tooltips.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------
function TomeTracker_Tooltip_Update()
end

if not TomeTracker or not TomeTracker.Journal then
		return -- the Tometracker Core messed up somewhere stop loading
end

TomeTracker.Tooltip = {}

--[===[@alpha@
TomeTracker.Tooltip.REV = 1
--@end-alpha@]===]
--@non-alpha@
TomeTracker.Tooltip.REV = 66
--@end-non-alpha@

TomeTracker.Tooltip.windowName = "TomeTracker_Tooltip"

TomeTracker.Tooltip.isShowing = false
TomeTracker.Tooltip.curMouseOverWindow = ""

TomeTracker.Tooltip.MAX_ROWS = 6
TomeTracker.Tooltip.MAX_COLS = 2
TomeTracker.Tooltip.MAX_WIDTH = 375
TomeTracker.Tooltip.BORDER_PADDING = {X = 10, Y = 5}
TomeTracker.Tooltip.ROW_SPACING = 5
TomeTracker.Tooltip.COL_SPACING = 10

TomeTracker.Tooltip.HEADING_COLOR = DefaultColor.ORANGE

TomeTracker.Tooltip.settings = {}

function TomeTracker.Tooltip.Initialize()

	d("Initializing TomeTracker Tooltips rev " .. TomeTracker.Tooltip.REV)

	CreateWindow(TomeTracker.Tooltip.windowName, false)
	--RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "TomeTracker.Tooltip.Update")
    WindowSetTintColor( TomeTracker.Tooltip.windowName .. "BackgroundInner", 20, 20, 20 )
    WindowSetAlpha( TomeTracker.Tooltip.windowName .. "BackgroundInner", .9 )

	-- replace with a working copy of the function if we init properly
	function TomeTracker_Tooltip_Update( timePassed )
		if( TomeTracker.Tooltip.isShowing ) then            
			if( TomeTracker.Tooltip.curMouseOverWindow ~= SystemData.MouseOverWindow.name or not SystemData.Settings.GamePlay.showToolTips ) then
				TomeTracker.Tooltip.ClearTooltip()
			end
		end
	end
	
end

function TomeTracker.Tooltip.ClearTooltip()

    WindowSetShowing( TomeTracker.Tooltip.windowName, false )     
    WindowClearAnchors ( TomeTracker.Tooltip.windowName )

		for rowNum = 1, TomeTracker.Tooltip.MAX_ROWS do
				local rowName = TomeTracker.Tooltip.windowName .. "Row" .. rowNum
				
				for colNum = 1, TomeTracker.Tooltip.MAX_COLS do
						local colName = rowName .. "Col" .. colNum
						WindowSetDimensions( colName , TomeTracker.Tooltip.MAX_WIDTH, 0 )
        		LabelSetText( colName, L"" )
        		LabelSetTextColor( colName, 255, 255, 255 ) 
				end
		end

		TomeTracker.Tooltip.isShowing = false

end

function TomeTracker.Tooltip.CreateTooltip( mouseOverWindow, tooltipData )
    
    TomeTracker.Tooltip.curMouseOverWindow = mouseOverWindow
    TomeTracker.Tooltip.ClearTooltip()    

		local tooltipText
		
		-- if we dont get a table deal with it
		if (tooltipData == nil) then
				tooltipText = {}
		elseif (type(tooltipData) ~= "table" ) then
				tooltipText = { { tooltipData } }
		else
				tooltipText = tooltipData
		end
		-- Populate tooltip from table data    
    if ( tooltipText ) then
    		for rowNum = 1, TomeTracker.Tooltip.MAX_ROWS do
    				if ( tooltipText[rowNum] ) then
								local rowName = TomeTracker.Tooltip.windowName .. "Row" .. rowNum
								for colNum = 1, TomeTracker.Tooltip.MAX_COLS do
										local colName = rowName .. "Col" .. colNum
										if tooltipText[rowNum][colNum] then
		        					LabelSetText( colName, towstring(tooltipText[rowNum][colNum]) )
		        					if (colNum == 1) then
		        							LabelSetTextColor( colName, TomeTracker.Tooltip.HEADING_COLOR.r, TomeTracker.Tooltip.HEADING_COLOR.g, TomeTracker.Tooltip.HEADING_COLOR.b )
		        					else
		        							LabelSetTextColor( colName, 255, 255, 255 )
		        					end
    		    				end
								end								    						
    				end
    		end
    end
    
    TomeTracker.Tooltip.ResizeTooltip()

		-- Anchor tooltip to mouse cursor
    local anchor = { Point = "bottomright",
    								 RelativeTo = "CursorWindow",
    								 RelativePoint = "topleft",
    								 XOffset = -50,
    								 YOffset = -25 }
    WindowAddAnchor( TomeTracker.Tooltip.windowName, anchor.Point, anchor.RelativeTo, anchor.RelativePoint, anchor.XOffset, anchor.YOffset ) 

		-- Show the tooltip
    WindowSetAlpha( TomeTracker.Tooltip.windowName, 1.0 )
    WindowSetShowing( TomeTracker.Tooltip.windowName, true )
    
    TomeTracker.Tooltip.isShowing = true
    
end

function TomeTracker.Tooltip.ResizeTooltip()

		local newHeight = 0
		local newWidth = 0
		local columnWidths = {}
		local rowHeights = {}
		local rowCount = 0
		
		-- Get final label and widow sizes
		for rowNum = 1, TomeTracker.Tooltip.MAX_ROWS do
				local rowWidth = 0
				local rowHeight = 0
				local rowName = TomeTracker.Tooltip.windowName .. "Row" .. rowNum
				local rowX, rowY = WindowGetDimensions( rowName )

				for colNum = 1, TomeTracker.Tooltip.MAX_COLS do
						local colName = rowName .. "Col" .. colNum
						local colWidth, colHeight = LabelGetTextDimensions( colName )
						local colX, colY = WindowGetDimensions( colName )
						
						-- Collect max row height
						if ( colHeight > rowHeight ) then
								rowHeight = colHeight
						end
						-- Remember widest colum size
						if ( colWidth > 0 and (columnWidths[colNum] == nil or colWidth > columnWidths[colNum]) ) then
								columnWidths[colNum] = colWidth
						end
						--d("Column (" .. rowNum .. "," .. colNum .. ") width = " .. colWidth .. " height = " .. colHeight)
				end
				--d(columnWidths)
				--d("Total Row width = " .. rowWidth)

				-- Collect row count, remember row heights and collect total window height
				if ( rowHeight > 0 ) then
						rowCount = rowCount + 1
						rowHeights[rowNum] = rowHeight
						newHeight = newHeight + rowHeight
				end
				
		end
		
		-- Resize columns and rows with largest values for neatly lined up titles and text
		for rowNum, rowHeight in pairs(rowHeights) do
				local rowName = TomeTracker.Tooltip.windowName .. "Row" .. rowNum
				newWidth = 0
				local colCount = 0
				for colNum, colWidth in pairs(columnWidths) do
						local colName = rowName .. "Col" .. colNum
						WindowSetDimensions( colName , colWidth, rowHeight)
						newWidth = newWidth + colWidth
						colCount = colCount + 1
				end
				if (colCount > 1) then
						newWidth = newWidth + ( TomeTracker.Tooltip.COL_SPACING * (colCount - 1) )
				end
				WindowSetDimensions( rowName , newWidth, rowHeight)
		end
		
		-- Add row height padding
		newHeight = newHeight + ( TomeTracker.Tooltip.ROW_SPACING * rowCount )

		-- Add border padding
		newWidth = newWidth + ( TomeTracker.Tooltip.BORDER_PADDING.X * 2 )
		newHeight = newHeight + ( TomeTracker.Tooltip.BORDER_PADDING.Y * 2 )
		
		--d("Final : width = " .. newWidth .. " height = " .. newHeight)
		-- Set final tooltip window size
		WindowSetDimensions( TomeTracker.Tooltip.windowName, newWidth, newHeight )

end