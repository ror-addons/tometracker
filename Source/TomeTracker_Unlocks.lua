--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Unlocks.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------

if not TomeTracker or not TomeTracker.Journal then
	if TomeTracker then
		TomeTracker.error("Unlocks Module not loaded")
	end
	return -- the Tometracker Journal messed up somewhere stop loading
end

local LibDateTime = LibStub("LibDateTime-Modified-0.4")

TomeTracker.Unlocks = {}

--[===[@alpha@
TomeTracker.Unlocks.REV = 1
--@end-alpha@]===]
--@non-alpha@
TomeTracker.Unlocks.REV = 66
--@end-non-alpha@

TomeTracker.Unlocks.windowName = "TTUnlocks"

TomeTracker.Unlocks.TextPage = [[
List of available Tome Unlocks from the Tome of Knowledge

** Coming Soon **

- See Help Tab for more info
]]

function TomeTracker.Unlocks.Initialize()

		d("Initializing Unlocks Tab rev " .. TomeTracker.Unlocks.REV)

		LabelSetText( TomeTracker.Unlocks.windowName .. "Text", towstring(TomeTracker.Unlocks.TextPage) )

end