--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Data.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------

if not TomeTracker then return end -- shortcut file loading if theres a problem with the core file

TomeTracker.Data = {}

-- Flag table, is location data for this section of the ToK common to all characters
TomeTracker.Data.Common = {}
TomeTracker.Data.Common[GameData.Tome.SECTION_BESTIARY]           = false -- 1
TomeTracker.Data.Common[GameData.Tome.SECTION_NOTEWORTHY_PERSONS] = true  -- 2
TomeTracker.Data.Common[GameData.Tome.SECTION_HISTORY_AND_LORE]   = true  -- 3
TomeTracker.Data.Common[GameData.Tome.SECTION_OLD_WORLD_ARMORY]   = false -- 4
TomeTracker.Data.Common[GameData.Tome.SECTION_WAR_JOURNAL]        = true  -- 20
TomeTracker.Data.Common[GameData.Tome.SECTION_PLAYER_TITLES]      = false -- 25
TomeTracker.Data.Common[GameData.Tome.SECTION_TACTICS]            = false -- 26
TomeTracker.Data.Common[GameData.Tome.SECTION_ACHIEVEMENTS]       = false -- 30
TomeTracker.Data.Common[GameData.Tome.SECTION_LIVE_EVENT]         = false -- 60

-- is this achievement type data common to all characters
TomeTracker.Data.AchievementType = {}
TomeTracker.Data.AchievementType[1] = false -- Realm vs Realm
TomeTracker.Data.AchievementType[2] = true  -- Exploration
TomeTracker.Data.AchievementType[3] = false -- Career
TomeTracker.Data.AchievementType[4] = false -- Tradeskills
TomeTracker.Data.AchievementType[5] = false -- Bestiary
TomeTracker.Data.AchievementType[6] = false -- Cities
TomeTracker.Data.AchievementType[7] = false -- Casual
TomeTracker.Data.AchievementType[8] = true  -- Pursuits
TomeTracker.Data.AchievementType[9] = false -- Guilds
