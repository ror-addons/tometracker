--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker_Settings.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------

if not TomeTracker or not TomeTracker.Journal then
	if TomeTracker then
		TomeTracker.error("Settings Module not loaded")
	end
	return -- the Tometracker Journal messed up somewhere stop loading
end

local LibDateTime = LibStub("LibDateTime-Modified-0.4")

TomeTracker.Settings = {}

--[===[@alpha@
TomeTracker.Settings.REV = 1
--@end-alpha@]===]
--@non-alpha@
TomeTracker.Settings.REV = 66
--@end-non-alpha@


TomeTracker.Settings.windowName = "TTSettings"

TomeTracker.Settings.TextPage = [[
Settings UI for TomeTracker Addon

** Coming Soon **

Planned features:
- Overhead Map button toggles for journal and killtracker
- Select box to choose the number of species tracked
- Filter menu to limit entries displayed in the saga list
- and more...

- See Help Tab for more info
]]

function TomeTracker.Settings.Initialize()
		
		d("Initializing Settings Tab rev " .. TomeTracker.Settings.REV)

		LabelSetText( TomeTracker.Settings.windowName .. "Text", towstring(TomeTracker.Settings.TextPage) )

end