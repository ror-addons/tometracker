--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/TomeTracker.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   MapPin Beta v0.3.5
-- Revision:  66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------

if not MapMonsterAPI.CheckVersion(0.3, 4) then
	d("Incorrect version of MapMonster, TomeTracker Shutting down")
	return
end

local LibToolkit = LibStub("LibToolkit-0.1")

TomeTracker = {}

TomeTracker.MAJOR_VER = 0.3
TomeTracker.MINOR_VER = 5
TomeTracker.VERSION = 2
TomeTracker.DATA_VERSION = 2
TomeTracker.DATA_LAYOUT = 3

TomeTracker.Errors = {}
TomeTracker.isWorldLoading = true
TomeTracker.PlayerIdentity = L""

TomeTracker.CHAT_LOG_FILTER = 888

--------------------------------------------------------------------------------
--#
--#			Local Functions
--#
--------------------------------------------------------------------------------

local characterListing = {}
	
local function DataLayoutConversion()

	--[===[@alpha@
	d("***Data Conversion***")
	--@end-alpha@]===]
	if not TomeTracker_Data then
		TomeTracker_Data = {}
	end
	-- Convert to version 1 data layout
	if not TomeTracker_Data.DATA_LAYOUT or TomeTracker_Data.DATA_LAYOUT == 0 then
		if not TomeTracker_Data.PlayerData then
			TomeTracker_Data.PlayerData = {}
		end
		for k, dataTable in pairs(TomeTracker_Data) do
			if k ~= "Options" and k ~= "TomeUnlocks" and k ~="PlayerData" and k ~="DATA_LAYOUT" then
				local split = StringSplit(k, "-")
				for i, v in pairs(split) do
					split[i] = LibToolkit.CleanString(v)
				end
				local serverName = split[1]
				local playerName = split[3]
				local realmName  = split[2]
				if realmName == "Destruction" then
					realmName = GameData.Realm.DESTRUCTION
				else
					realmName = GameData.Realm.ORDER
				end
				-- Create Pins from Data
				if not TomeTracker_Data.PlayerData[serverName] then
					TomeTracker_Data.PlayerData[serverName] = {}
					TomeTracker_Data.PlayerData[serverName].Realm = realmName
				end
				TomeTracker_Data.PlayerData[serverName][playerName] = dataTable
				TomeTracker_Data[k] = nil
			end
		end
		TomeTracker_Data.DATA_LAYOUT = 1
		DataLayoutConversion()
	-- Global clean up of sotred data
	elseif TomeTracker_Data.DATA_LAYOUT == 1 then
		-- clean up tome unlock labels
		for k, tbl in pairs(TomeTracker_Data.TomeUnlocks) do
			-- if we have a label then clean it
			if tbl.label then
				TomeTracker_Data.TomeUnlocks[k].label = LibToolkit.CleanWString( towstring(tbl.label) )
			end
		end
		-- clean up all players data
		for server, serverData in pairs(TomeTracker_Data.PlayerData) do
			-- check each player for server
			for player, playerData in pairs(serverData) do
				if type(playerData) == "table" then
					playerData.KillTracker = nil
					-- clean up the player Epic
					for unlock, unlockData in pairs(playerData.Epic) do
						if unlockData.label then
							unlockData.label = LibToolkit.CleanWString ( unlockData.label )
						end
						if unlockData.locString then
							unlockData.locString = LibToolkit.CleanWString ( unlockData.locString )
						end
					end
				end
			end
		end
		TomeTracker_Data.DATA_LAYOUT = 2
		DataLayoutConversion()
	-- Clean up existing map pins
	elseif TomeTracker_Data.DATA_LAYOUT == 2 then
		local myPinTypes = { TomeTracker.Pins.CHARACTER_STATIC,
							 TomeTracker.Pins.CHARACTER_VARIABLE,
							 TomeTracker.Pins.STATIC,
							 TomeTracker.Pins.VARIABLE,}
		-- run through each of our pintypes
		for k, pinType in pairs(myPinTypes) do
			local pinList = MapMonsterAPI.GetPinsByType(pinType)
			if pinList then
				for id, pinData in pairs(pinList) do
					if wstring.match(pinData.label, L"%^") then
						local newLabel = LibToolkit.CleanWString(pinData.label)
						MapMonsterAPI.SetPinProperty(pinData.id, "label", newLabel, false)
					end
				end
			end
		end
		TomeTracker_Data.DATA_LAYOUT = 3
		DataLayoutConversion()
	end
end

local function RemoveOrphanedData()
	--[===[@alpha@
	d("Remove Orphaned Data")
	--@end-alpha@]===]
	for k, characterData in pairs(GameData.Account.CharacterSlot) do
		if characterData.Name ~= nil and characterData.Name ~= L"" then
			local cleanName = LibToolkit.CleanWString(characterData.Name)
			characterListing[WStringToString(cleanName)] = true
		end
	end

	-- check current server data
	if not TomeTracker_Data.PlayerData or not TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName] then
		return -- skip if we have no data for this server
	end
	for characterName, characterData in pairs(TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName]) do
		if characterName ~= "Realm" then
			if not characterListing[characterName] then -- not a current character
				if not TomeTracker_OrphanedData.MissingCharacterData[LibToolkit.Clean.ServerName] then
					TomeTracker_OrphanedData.MissingCharacterData[LibToolkit.Clean.ServerName] = {}
				end
				TomeTracker_OrphanedData.MissingCharacterData[LibToolkit.Clean.ServerName][characterName] = TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName][characterName]
				TomeTracker_OrphanedData.MissingCharacterData[LibToolkit.Clean.ServerName][characterName].Realm = TomeTracker_OrphanedData.MissingCharacterData[LibToolkit.Clean.ServerName].Realm
				TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName][characterName] = nil
			end
		end
	end
end

--------------------------------------------------------------------------------
--#
--#			Global Functions and Event Handlers
--#
--------------------------------------------------------------------------------

function TomeTracker.Initialize()

	d("Initializing TomeTracker v" .. TomeTracker.MAJOR_VER .. "." .. TomeTracker.MINOR_VER)

	-- Switch saved vars to new var
	if TomeTracker_Data == nil then
		if TomeTracker.SavedVariables then
			TomeTracker_Data = TomeTracker.SavedVariables
			TomeTracker.SavedVariables = nil
		else
			TomeTracker_Data = {}
		end
	end
	if TomeTracker_OrphanedData == nil then
		TomeTracker_OrphanedData = {}
		TomeTracker_OrphanedData.MissingCharacterData = {}
	end

	-- make theres no left over migration tag
	TomeTracker_Data.migrateToMapMonster = nil

	if TomeTracker_Data.TomeUnlocks then
		DataLayoutConversion()
		RemoveOrphanedData()
	else
		TomeTracker_Data.DATA_LAYOUT = TomeTracker.DATA_LAYOUT
	end
	
	
	TomeTracker.InitSavedVariables()

	TomeTracker.Pins.Initialize()

	RegisterEventHandler(SystemData.Events.LOADING_BEGIN, "TomeTracker.OnLoadingBegin")
	RegisterEventHandler(SystemData.Events.LOADING_END, "TomeTracker.OnLoadingEnd")
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "TomeTracker.OnLoadingEnd")
	
	LibToolkit.NewChatFilter(TomeTracker.CHAT_LOG_FILTER, DefaultColor.ORANGE)

	-- Create and init journal window
	if TomeTracker.Journal then
		TomeTracker.Journal.Initialize()
		TomeTracker.Tooltip.Initialize()
	else
		error("Unable to initialize TomeTracker Journal")
	end

end


function TomeTracker.InitSavedVariables()

	-- Shared Addon Options
	if (TomeTracker_Data.Options == nil) then
		TomeTracker_Data.Options = {
									VERSION = TomeTracker.VERSION,
									showMapButton = true,
									lastCharacter = "",
									}
	end

	-- Check for mapbutton option and fix it
	if (TomeTracker_Data.Options.showMapButton == nil) then
		TomeTracker_Data.Options.showMapButton = true
	end

	-- Check for lastCharacter
	if (TomeTracker_Data.Options.lastCharacter == nil) then
		TomeTracker_Data.Options.lastCharacter = ""
		TomeTracker_Data.Options.lastServer = ""
		TomeTracker_Data.Options.lastRealm = 0
	end

	-- Shared Unlock list
	if (TomeTracker_Data.TomeUnlocks == nil) then
		TomeTracker_Data.TomeUnlocks = {}
		TomeTracker_Data.PlayerData = {}
	end

	-- Set up initial player vars
	if not TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName] then
		TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName] = {}
		TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName].Realm = GameData.Player.realm
	end
	if (TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName][LibToolkit.Clean.PlayerName] == nil) then
		TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName][LibToolkit.Clean.PlayerName] = {
						DATA_VERSION = TomeTracker.DATA_VERSION,
						level = 0,
						renown = 0,
						Chronology = {},
						Epic = {},
						}
	end

	TomeTracker.Player = TomeTracker_Data.PlayerData[LibToolkit.Clean.ServerName][LibToolkit.Clean.PlayerName]
	TomeTracker.Options = TomeTracker_Data.Options
	TomeTracker.TomeUnlocks = TomeTracker_Data.TomeUnlocks

	TomeTracker.ValidateSavedVariables()

end

function TomeTracker.ValidateSavedVariables()

		-- Check current addon version
		if not TomeTracker.Options.VERSION then
				TomeTracker.Options.VERSION = TomeTracker.VERSION
		end

		-- Check current data version
		if not TomeTracker.Player.DATA_VERSION then
				TomeTracker.Player.DATA_VERSION = TomeTracker.DATA_VERSION
		end

		--
		-- reserved space to include data version migration if we implement changes later
		--
		
		-- if were still using data version 1 migrate it all to MapMonster during pin init
		if TomeTracker.Player.DATA_VERSION == 1 then
			--[===[@alpha@
			d("Data Version : " .. TomeTracker.Player.DATA_VERSION)
			--@end-alpha@]===]
			TomeTracker_Data.migrateToMapMonster = true
		end
		
		-- check saved vars for proper type
		local verifyList = {
							DATA_VERSION = "number",
							level = "number",
							renown = "number",
							Chronology = "table",
							Epic = "table",
					}
		
		for varName, varType in pairs(verifyList) do
				if (TomeTracker.Player[varName] == nil or type(TomeTracker.Player[varName]) ~= varType) then
						if (varType == "number") then
								TomeTracker.Player[varName] = 0
						elseif (varType == "table") then
								TomeTracker.Player[varName] = {}
						elseif (varType == "string") then
								TomeTracker.Player[varName] = ""						
						elseif (varType == "wstring") then
								TomeTracker.Player[varName] = L""						
						end
				end
		end
		
		-- drop any saved vars that dont belong
		for varName, value in pairs(TomeTracker.Player) do
				if not verifyList[varName] then
						TomeTracker.Player[varName] = nil
				end
		end
		
end

--
-- Fired Event functions
--

function TomeTracker.OnLoadingBegin()
		TomeTracker.isWorldLoading = true
end

function TomeTracker.OnLoadingEnd()
	TomeTracker.isWorldLoading = false
end

function TomeTracker.OnShutdown()

end

--
-- Shared Internal Utility Functions
--

function TomeTracker.PrintStatus()

		TomeTracker.PrintVersion()
		TomeTracker.print("========================")
		-- if we have loading errors print them here with the version
		if TomeTracker.Errors[1] then
				for i, errorString in ipairs(TomeTracker.Errors) do
						TomeTracker.print(" - " .. errorString)
				end
		else
				TomeTracker.print("All modules loaded successfully")
		end
		-- check the tabs all exist
		local tabError = false
		if ( TomeTracker.Journal ~= nil and TomeTracker.Journal.Tabs ~= nil) then
				for tabName, tabData in pairs(TomeTracker.Journal.Tabs) do
            local exists = DoesWindowExist( tabData.window )
            if not exists then
            		TomeTracker.print(tabData.label .. " Tab NOT Loaded")
            		tabError = true
            end
				end
		end
		if not tabError then
				TomeTracker.print("All Journal tabs loaded successfully")
		end

end

function TomeTracker.PrintVersion()
		local major, minor = TomeTracker.GetVersion()
		
		TomeTracker.print("TomeTracker Ver. " .. major .. "." .. minor)

end

function TomeTracker.GetVersion()
		return TomeTracker.MAJOR_VER, TomeTracker.MINOR_VER
end

function TomeTracker.error(txt)

		table.insert(TomeTracker.Errors, txt)
		TomeTracker.print(txt)

end
function TomeTracker.print(txt)
	LibToolkit.print(TomeTracker.CHAT_LOG_FILTER, "[TomeTracker]", txt)
end

function fast_assert(condition, ...)
    if not condition then
        if getn(arg) > 0 then
            assert(condition, call(format, arg))
        else
            assert(condition)
        end
    end
end

