2009-01-13  Bloodwalker  <Bloodwalker>

	* Source/TomeTracker.lua, Source/TomeTracker_Pins.lua:
	FIXED: Data conversions would prevent addon from loading if they
	have no previous data
	[68cd293b6825] [MapPin Beta v0.3.5]

