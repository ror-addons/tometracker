<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="TomeTracker_Reporter" version="0.1" date="09/10/2008">
		<Author name="Bloodwalker" email="metagamegeek@gmail.com" />
		<Description text="Simple Reporting Function for TomeTracker" />
		<Dependencies>
			<Dependency name="TomeTracker"/>
		</Dependencies>
		<Files>
			<File name="TomeTracker_Reporter.lua" />
		</Files>
		<OnInitialize>
			<CallFunction name="TomeTracker_Reporter.Initialize" />
		</OnInitialize>
		<SavedVariables>
			<SavedVariable name="TomeTracker_Reporter.Data" />
			<SavedVariable name="TomeTracker_Reporter.Positions" />
		</SavedVariables>
	</UiMod>
</ModuleFile>