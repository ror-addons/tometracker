--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      TomeTracker_Reporter.lua
-- Date:      2009-01-13T21:11:41Z
-- Author:    Bloodwalker
-- Version:   66
-- File Rev:  66
-- Copyright: 2008
--------------------------------------------------------------------------------

TomeTracker_Reporter = {}

function TomeTracker_Reporter.Initialize()
		TomeTracker_Reporter.Data = {}
		if TomeTracker_Reporter.Positions == nil then
				TomeTracker_Reporter.Positions ={}
		end
end

function TomeTracker_Reporter.ReportData(object)

		d("Saving Output to TomeTracker_Reporter.Data")
		-- just stuff in the saved table raw
		table.insert(TomeTracker_Reporter.Data, object)

end

function TomeTracker_Reporter.PlayerPosition()
		
		local playerPos = LibMapPins.GetPlayerPosition()
		if not playerPos.worldX or not playerPos.worldY then
				TomeTracker.print("Player Position: You have to move first")
				return
		end
		local posString = "zoneId = " .. playerPos.zoneId .. " : X = " .. playerPos.worldX .. " : Y = " .. playerPos.worldY
		TomeTracker.print("Player Position: " .. posString)
		table.insert(TomeTracker_Reporter.Positions, playerPos)

end

function TomeTracker_Reporter.InGameSelfTest()

		-- test the Gamedata variables were expecting to have
		local gameDataVars = {
							{ name = "GameData.Player.name",                     varType = "wstring", condition = "not",  compare = L""},
							{ name = "GameData.Player.level",                    varType = "number",  condition = "gt",   compare = 0},
							{ name = "GameData.Player.Renown.curRank",           varType = "number",  condition = "gte",  compare = 0},
							{ name = "GameData.Account.ServerName",              varType = "wstring", condition = "not",  compare = L""},

							{ name = "GameData.Bestiary.updatedSpecies",         varType = "number",  condition = "gt",   compare = 0},

							{ name = "GameData.Realm.DESTRUCTION",               varType = "number",  value = 2 },
							{ name = "GameData.Realm.ORDER",                     varType = "number",  value = 1 },

							{ name = "GameData.Tome.SECTION_BESTIARY",           varType = "number",  value = 1 },
							{ name = "GameData.Tome.SECTION_NOTEWORTHY_PERSONS", varType = "number",  value = 2 },
							{ name = "GameData.Tome.SECTION_HISTORY_AND_LORE",   varType = "number",  value = 3 },
							{ name = "GameData.Tome.SECTION_OLD_WORLD_ARMORY",   varType = "number",  value = 4 },
							{ name = "GameData.Tome.SECTION_WAR_JOURNAL",        varType = "number",  value = 20 },
							{ name = "GameData.Tome.SECTION_PLAYER_TITLES",      varType = "number",  value = 25 },
							{ name = "GameData.Tome.SECTION_TACTICS",            varType = "number",  value = 26 },
							{ name = "GameData.Tome.SECTION_ACHIEVEMENTS",       varType = "number",  value = 30 },
							{ name = "GameData.Tome.SECTION_ZONE_MAPS",          varType = "number",  value = 50 },
							{ name = "GameData.Tome.SECTION_GAME_MANUAL",        varType = "number",  value = 100 },
							{ name = "GameData.Tome.SECTION_GAME_FAQ",           varType = "number",  value = 102 },
							{ name = "GameData.Tome.REWARD_XP",                  varType = "number",  value = 0 },
							{ name = "GameData.Tome.REWARD_TITLE",               varType = "number",  value = 1 },
							{ name = "GameData.Tome.REWARD_QUEST",               varType = "number",  value = 2 },
							{ name = "GameData.Tome.REWARD_ABILITY",             varType = "number",  value = 3 },
							{ name = "GameData.Tome.REWARD_ITEM",                varType = "number",  value = 4 },
							{ name = "GameData.Tome.REWARD_ABILITY_COUNTER",     varType = "number",  value = 6 },
							{ name = "GameData.Tome.REWARD_ITEM_NO_AUTOCREATE",  varType = "number",  value = 7 },
		}
		
		-- test the WH API functions we need
		local gameAPIFuncs = {
							{ name = "DataUtils.GetTomeAlerts",       returnType = "table", },
							{ name = "TomeGetBestiarySpeciesData",    returnType = "table",    args = 47 }, -- empire
							{ name = "TomeIsBestiarySpeciesUnlocked", returnType = "boolean",  args = 47 },
							{ name = "GetZoneName",                   returnType = "wstring",  args = 100 }, -- norsca
							{ name = "TomeGetPlayerTitleData",        returnType = "table",    args = 630 }, -- the friendly
 							{ name = "TomeGetItemRewardData",         returnType = "table",    args = 70 }, -- shawl of sprig
							{ name = "TomeGetTacticCounter",          returnType = "multiple", args = 334, returnList = {"wstring", "number", "table"} }, -- chaos tactic fragment
							{ name = "TomeGetTacticRewardsList",      returnType = "table", }, -- unlocked tome tactics list
		}
		
		-- test table structures returned from WH API functions
		local gameFuncReturn = {
							{ name = "DataUtils.GetTomeAlerts",    multiple = true, structure = TomeTracker_Reporter.TomeAlertsReturn },
							{ name = "TomeGetBestiarySpeciesData", args = 47,       structure = TomeTracker_Reporter.TomeGetBestiarySpeciesDataReturn },
							{ name = "TomeGetPlayerTitleData",     args = 630,      structure = TomeTracker_Reporter.TomeGetPlayerTitleDataReturn },
							{ name = "TomeGetItemRewardData",      args = 70,       structure = TomeTracker_Reporter.TomeGetItemRewardDataReturn },
							{ name = "TomeGetTacticRewardsList",   multiple = true, structure = TomeTracker_Reporter.TomeGetTacticRewardsListReturn },
		}
		
		local totalTests = 0
		local totalFailed = 0
		
		TomeTracker.print("Begin In-Game Self Testing:")

		TomeTracker.print("1. Testing Game Variables:")
		
		local varTestCount = 0
		local varTestFailed = 0		

		for i, varEntry in ipairs(gameDataVars) do

				varTestCount = varTestCount + 1
				local varValue = TomeTracker_Reporter.GetGlobalValue(varEntry.name)

				-- Test existance
				if (varValue == nil) then
						TomeTracker.print(varEntry.name .. " Failed! Doesn't exist")
						varTestFailed = varTestFailed + 1
				-- Test expected type
				elseif (type(varValue) ~= varEntry.varType) then
						TomeTracker.print(varEntry.name .. " Failed! Expected " .. varEntry.varType .. " got " .. type(varValue))
						varTestFailed = varTestFailed + 1
				-- Test expected value or conditional
				else
						if varEntry.value then
								if (varValue ~= varEntry.value) then
										TomeTracker.print(varEntry.name .. " Failed! Expected " .. tostring(varEntry.value) .. " got " .. tostring(varValue))
										varTestFailed = varTestFailed + 1
								end
						elseif varEntry.condition then
								if (varEntry.condition == "not") then
										if (varValue == varEntry.compare) then
												TomeTracker.print(varEntry.name .. " Failed! Variable doesn't satisfy conditional")
												varTestFailed = varTestFailed + 1
										end
								elseif (varEntry.condition == "gt") then
										if (varValue <= varEntry.compare) then
												TomeTracker.print(varEntry.name .. " Failed! Variable not greater than " .. tostring(varEntry.compare))
												varTestFailed = varTestFailed + 1
										end								
								elseif (varEntry.condition == "gte") then
										if (varValue < varEntry.compare) then
												TomeTracker.print(varEntry.name .. " Failed! Variable not equal to or greater than " .. tostring(varEntry.compare))
												varTestFailed = varTestFailed + 1
										end																
								end
						end
				end

		end
		TomeTracker.print("- Tests: " .. varTestCount .. " Passed: " .. varTestCount - varTestFailed .. " Failed: " .. varTestFailed)
		
		totalTests = totalTests + varTestCount
		totalFailed = totalFailed + varTestFailed
		
		TomeTracker.print("2. WH API Function Tests:")
		
		local funcTestCount = 0
		local funcTestFailed = 0
		
		for i, funcEntry in ipairs(gameAPIFuncs) do

				funcTestCount = funcTestCount + 1
				local funcRef = TomeTracker_Reporter.GetGlobalValue(funcEntry.name)
				local funcReturn
				local funcArgs
		
				if funcEntry.args then
						funcArgs = funcEntry.args
				else
						funcArgs = nil
				end
				
				-- test functions that return multiple values
				if (funcEntry.returnType == "multiple") then						
						local multipleFail = false
						TTR_a1, TTR_a2, TTR_a3, TTR_a4, TTR_a5, TTR_a6 = funcRef(funcArgs)
						
						for i = 1, 6 do
								if (funcEntry.returnList[i] ~= nil) then
										local aReturn = TomeTracker_Reporter.GetGlobalValue("TTR_a" .. i)
										if (type(aReturn) ~= funcEntry.returnList[i]) then
												TomeTracker.print(funcEntry.name .. "() Failed! Expected " .. funcEntry.returnList[i] .. " got " .. type(aReturn))
												multipleFail = true
										end
								end
						end
						
						if multipleFail then
								funcTestFailed = funcTestFailed + 1
						end
				-- test functions that return a single value
				else
						funcReturn = funcRef(funcArgs)
						
						if (type(funcReturn) ~= funcEntry.returnType) then
								TomeTracker.print(funcEntry.name .. "() Failed! Expected " .. funcEntry.returnType .. " got " .. type(funcReturn))
								funcTestFailed = funcTestFailed + 1
						end
				end

		end

		TomeTracker.print("- Tests: " .. funcTestCount .. " Passed: " .. funcTestCount - funcTestFailed .. " Failed: " .. funcTestFailed)

		totalTests = totalTests + funcTestCount
		totalFailed = totalFailed + funcTestFailed

		TomeTracker.print("3. WH API Function Return Values Tests:")
		
		local returnTestCount = 0
		local returnTestFailed = 0

		for i, returnTest in ipairs(gameFuncReturn) do

				returnTestCount = returnTestCount + 1
				local funcRef = TomeTracker_Reporter.GetGlobalValue(returnTest.name)
				local funcReturn
				local funcArgs
				local funcStruct = returnTest.structure
		
				if returnTest.args then
						funcArgs = returnTest.args
				else
						funcArgs = nil
				end
		
				funcReturn = funcRef(funcArgs)
				
				-- test the table structure and types not the values
				-- we get a table full of tables
				if returnTest.multiple then
						local multipleFailed = false
						for i, returnedValue in ipairs(funcReturn) do
								-- test the struct here
								local sameStruct = TomeTracker_Reporter.CompareTableKeysAndTypes(funcStruct, returnedValue)
								if not sameStruct then
										TomeTracker.print(returnTest.name .. "() Failed! Table keys and types do not match expected values")
										multipleFailed = true
										break -- skip the rest
								end
						end
						if multipleFailed then
								returnTestFailed = returnTestFailed + 1
						end
				else
						-- test struct here
						local sameStruct = TomeTracker_Reporter.CompareTableKeysAndTypes(funcStruct, funcReturn)
						if not sameStruct then
								TomeTracker.print(returnTest.name .. "() Failed! Mismatched table structure of return value ")
								returnTestFailed = returnTestFailed + 1
						end
				end
		
		end

		TomeTracker.print("- Tests: " .. returnTestCount .. " Passed: " .. returnTestCount - returnTestFailed .. " Failed: " .. returnTestFailed)

		totalTests = totalTests + returnTestCount
		totalFailed = totalFailed + returnTestFailed
		
		TomeTracker.print("In-Game Self Testing Finished! Tests: " .. totalTests .. " Passed: " .. totalTests - totalFailed .. " Failed: " .. totalFailed)

end

function TomeTracker_Reporter.GetGlobalValue(f)

      local v = _G    -- start with the table of globals

      for w in string.gfind(f, "[%w_]+") do
        v = v[w]
      end

      return v

end

function TomeTracker_Reporter.CompareTableKeysAndTypes(firstTable, secondTable)

		-- not even tables
		if (firstTable == nil or type(firstTable) ~= "table" or
				secondTable == nil or type(secondTable) ~= "table")
		then
				return false
		end

		-- get the length of each table and compare
		local firstLength = 0
		for key, value in pairs(firstTable) do
				firstLength = firstLength + 1
		end
		local secondLength = 0
		for key, value in pairs(secondTable) do
				secondLength = secondLength + 1
		end
		-- check for identical length
		if (firstLength ~= secondLength) then
				return false
		end

		-- we have 2 tables and they are the same size, now lets compare key names and types
		for key, value in pairs(firstTable) do
				-- compare the type from the second table to match the type of this value
				if (type(secondTable[key]) ~= type(value)) then
						return false
				end
		end
		
		-- if we made it this far its all good
		return true

end

--
-- Data Copied from In Game - This is what we are expecting to see
--

TomeTracker_Reporter.TomeAlertsReturn = {
			alertType = 1,
			id = 2509,
			subEntry = 161,
			desc = L"You have learned about The Eternal Citadel",
			rewardType = 0,
			name = L"The Eternal Citadel",
			section = 3,
			suppressPopup = false,
			rewardId = 0,
			xp = 500,
			entry = 320,
		}

TomeTracker_Reporter.TomeGetBestiarySpeciesDataReturn = 	{
		text2 = L"The life of a citizen of the Empire is a perilous one, for there are foes within every shadow and enemies around every corner. Secret cults abound, or at the very least, rumors of such cults run rampant. No one is certain who to trust, and fear and paranoia are omnipresent in the isolated settlements that dot the Empire's provinces.<P>As the Empire rots from within, it is constantly under assault from without. Savage barbarians from the north mount massive campaigns into the Empire, pillaging and destroying all that they can. In the Age of Reckoning, one such warhost has attacked the Empire's northern borders under the leadership of a powerful Champion. The timing of the invasion is particularly bad, for the Empire is also being ravaged by a terrible plague.",
		name = L"Empire",
		text1 = L"The Empire is composed of the human descendants of Sigmar, who united the tribes of men at the Battle of Black Fire Pass. Sigmar was deified after the battle, and his promise of eternal aid to the kingdom of Dwarfs still stands today. This action solidified the relationship between men and Dwarfs, and planted the seeds of the burgeoning Empire through trade. Today, the Empire is led by Emperor Karl Franz, who rules from his seat in the city-state of Altdorf.",
		tasks = 
		{
			
			{
				unlockEventId = 3460,
				isComplete = true,
				rewards = 
				{
					
					{
						rewardId = 84,
						rewardType = 0,
					},
					
					{
						rewardId = 0,
						rewardType = 5,
					},
				},
				cardId = 0,
				text = L"Encounter a member of the Empire",
				desc = L"",
			},
			
			{
				unlockEventId = 3461,
				isComplete = true,
				rewards = 
				{
					
					{
						rewardId = 204,
						rewardType = 0,
					},
					
					{
						rewardId = 0,
						rewardType = 5,
					},
				},
				cardId = 0,
				text = L"Kill 25 members of the Empire",
				desc = L"",
			},
			
			{
				isComplete = false,
				rewards = 
				{
					
					{
						rewardId = 336,
						rewardType = 0,
					},
					
					{
						rewardId = 97,
						rewardType = 1,
					},
				},
				text = L"Complete an Empire Scenario",
				cardId = 0,
				desc = L"Enter an Empire Scenario",
			},
			
			{
				unlockEventId = 3463,
				isComplete = true,
				rewards = 
				{
					
					{
						rewardId = 500,
						rewardType = 0,
					},
					
					{
						rewardId = 0,
						rewardType = 5,
					},
				},
				cardId = 0,
				text = L"Kill 100 members of the Empire",
				desc = L"",
			},
			
			{
				unlockEventId = 3464,
				isComplete = true,
				rewards = 
				{
					
					{
						rewardId = 806,
						rewardType = 0,
					},
					
					{
						rewardId = 94,
						rewardType = 7,
					},
				},
				cardId = 0,
				text = L"Kill 1,000 members of the Empire",
				desc = L"",
			},
			
			{
				rewards = 
				{
				},
				text = L"The History of the Empire",
				desc = L"Perform a specific task to learn about the Empire",
				isComplete = false,
			},
			
			{
				rewards = 
				{
				},
				text = L"?",
				desc = L"",
				isComplete = false,
			},
			
			{
				rewards = 
				{
				},
				text = L"?",
				desc = L"",
				isComplete = false,
			},
			
			{
				rewards = 
				{
				},
				text = L"?",
				desc = L"",
				isComplete = false,
			},
			
			{
				rewards = 
				{
				},
				text = L"?",
				desc = L"",
				isComplete = false,
			},
		},
		text3 = L"Faced with such desperate circumstances, Emperor Karl Franz has been forced to call upon the Dwarfs and High Elves for aid, lest his lands be transformed forever into a nightmarish domain of Chaos.",
		killCount = 1307,
		id = 47,
	}

TomeTracker_Reporter.TomeGetPlayerTitleDataReturn = 	{
		type = 6,
		name = L"The Friendly^m",
		id = 630,
		text = L"",
		unlockInfo = 
		{
			entry = 129,
			text = L"You have added 5 characters on your friends list",
			name = L"Added 5 characters on your Friends list",
			section = 30,
		},
	}

TomeTracker_Reporter.TomeGetItemRewardDataReturn = 	{
		description = L"Reward for completing specific bestiary unlock",
		iLevel = 0,
		maxEquip = 0,
		slots = 
		{
		},
		equipSlot = 13,
		races = 
		{
		},
		tintB = 0,
		stackCount = 1,
		level = 0,
		blockRating = 0,
		iconNum = 3673,
		rarity = 2,
		marketingVariation = 0,
		flags = 
		{
			false,
			false,
			true,
			false,
			false,
			false,
			false,
			false,
			true,
			false,
			false,
			false,
			false,
			false,
			false,
		},
		name = L"Shawl of Spring",
		broken = false,
		skills = 
		{
		},
		unlockInfo = 
		{
			entry = 40,
			text = L"You have completed: Death in Green and Brown",
			name = L"Death in Green and Brown",
			section = 1,
		},
		uniqueID = 192316,
		sellPrice = 0,
		craftingSkillRequirement = 0,
		cultivationType = 0,
		dyeTintB = 0,
		dyeTintA = 0,
		currChargesRemaining = 0,
		tintA = 0,
		isNew = true,
		enhSlot = 
		{
		},
		bonus = 
		{
		},
		numEnhancementSlots = 0,
		type = 35,
		boundToPlayer = false,
		marketingIndex = 0,
		id = 6840,
		dps = 0,
		craftingBonus = 
		{
		},
		trophyLocIndex = 1,
		careers = 
		{
		},
		speed = 0,
		renown = 0,
		noChargeLeftDontDelete = 0,
		bop = true,
		armor = 0,
		trophyLocation = 0,
		itemSet = 0,
	}

TomeTracker_Reporter.TomeGetTacticRewardsListReturn = {
			rewardType = 6,
			name = L"Queek Reek^n",
			iconNum = 22716,
			rewardId = 22,
			abilityId = 15121,
		}