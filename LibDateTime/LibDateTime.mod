<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	
    <UiMod name="LibDateTime-0.4.6" version="0.4.6" date="26/10/2008"  autoenabled="false">
        <Author name="Garko" email="yann.lugrin@sans-savoir.net" />
        <Description text="LibDateTime provide locale date and time, formatting function, compare function and clock callback." />
<!-- Disbaled and relocated -->

    </UiMod>
    
</ModuleFile>